# buses-ecuador-backend

## Prerrequisitos para levantar el backend

- Java 17 

Para verificar que se esté usando la versión correcta ejecutar en la terminal del IDE que se esté usando:
```shell script
java -version
```
- Docker Desktop

Para correr el backend, esta aplicación debe estar abierta.

- IntellijIDEA (Opcional, es un IDE de desarrollo)
- Oracle SQL Developer (Opcional, solo si quieren ver las tablas desde ahí)

## Para correr la aplicación

Abrir una terminal en la raiz del proyecto y ejecutar:

```shell script
docker-compose -f src/main/docker/oracle.yml up
```
Una vez que en la terminar diga "Database ready to use. Enjoy ;)" dejamos en segundo plano esta terminal y abrimos otra.

En la otra terminal ejecutamos:

```shell script
./mvnw compile quarkus:dev
```

Esto deberá levantar correctamente el backend y se podrán consumir los servicios desde 
```shell script
http://localhost:8080
```
