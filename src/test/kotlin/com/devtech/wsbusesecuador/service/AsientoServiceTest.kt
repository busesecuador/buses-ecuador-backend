package com.devtech.wsbusesecuador.service

import com.devtech.wsbusesecuador.domain.Asiento
import com.devtech.wsbusesecuador.repository.AsientoRepository
import io.quarkus.test.TestTransaction
import io.quarkus.test.junit.QuarkusTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import javax.inject.Inject

@QuarkusTest
@TestTransaction
class AsientoServiceTest {

    @Inject
    lateinit var asientoService: AsientoService

    @Inject
    lateinit var asientoRepository: AsientoRepository

    // OBTENER TODOS LOS REGISTROS DE LA TABLA

    @Test
    fun `debe retornar vacio cuando en la tabla no hay registros`() {
        val asientoList = asientoService.obtenerAsientos()
        assertTrue(asientoList.isNullOrEmpty())
    }

    @Test
    fun `debe retornar una lista de asientos`() {
        asientoRepository.persist(Asiento(123456, tipoAsien = "NORMAL"))
        asientoRepository.persist(Asiento(654321, tipoAsien = "VIP"))

        val asientoList = asientoService.obtenerAsientos()

        assertEquals(2, asientoList!!.size)
        assert(asientoList.any { it.idAsien == 654321L })
        assert(asientoList.any { it.idAsien == 123456L })
    }

    // OBTENER REGISTRO POR SU ID

    @Test
    fun `debe retornar nulo cuando en la tabla no hay registros`() {
        val asiento = asientoService.obtenerAsientoPorID(123456)
        assertTrue(asiento == null)
    }

    @Test
    fun `debe retornar nulo cuando en la tabla no se encuentra el asiento`() {
        asientoRepository.persist(Asiento(123456, tipoAsien = "NORMAL"))

        val asiento = asientoService.obtenerAsientoPorID(654321)
        assertTrue(asiento == null)
    }

    @Test
    fun `debe retornar un asiento por su ID`() {
        asientoRepository.persist(Asiento(123456, tipoAsien = "NORMAL"))

        val asiento = asientoService.obtenerAsientoPorID(123456)

        assertTrue(asiento?.idAsien == 123456L)
        assertTrue(asiento?.tipoAsien == "NORMAL")
    }

    // OBTENER ULTIMO ID

    @Test
    fun `debe retornar nulo si no encuentra registros`() {

        val maxID = asientoService.obtenerMaximoID()

        assertTrue(maxID == null)
    }

    @Test
    fun `debe retornar el numero de ID maximo`() {
        asientoRepository.persist(Asiento(123456, tipoAsien = "NORMAL"))
        asientoRepository.persist(Asiento(21323, tipoAsien = "NORMAL"))
        asientoRepository.persist(Asiento(1456, tipoAsien = "NORMAL"))

        val maxID = asientoService.obtenerMaximoID()

        assertTrue(maxID == 123456L)
    }

    // INSERTAR UN NUEVO REGISTRO

    @Test
    fun `debe retornar nulo si no se pudo ingresar`() {
        val asientoInsertado = asientoService.insertarAsiento(null)

        val asientoList = asientoService.obtenerAsientos()

        assertTrue(asientoInsertado == null)

        assertTrue(asientoList!!.isEmpty())
    }

    @Test
    fun `debe insertar un asiento con el ID maximo mas uno y retornarla`() {
        asientoRepository.persist(Asiento(123456, tipoAsien = "NORMAL"))

        val asientoNuevo = Asiento(1, tipoAsien = "NORMAL")

        val asientoInsertado = asientoService.insertarAsiento(asientoNuevo)

        val asientoList = asientoService.obtenerAsientos()

        assertTrue(asientoInsertado?.idAsien == 123457L)

        assertTrue(asientoList!!.size == 2)
        assert(asientoList.any { it.idAsien == 123457L })
        assert(asientoList.any { it.idAsien == 123456L })
    }

    // ACTUALIZAR UN REGISTRO

    @Test
    fun `debe retornar nulo cuando no encuentre el registro que se quiere actualizar`() {

        asientoRepository.persist(Asiento(123456, tipoAsien = "NORMAL"))

        val asientoNuevosDatos = Asiento(123457, tipoAsien = "NORMAL")

        val asientoActualizado = asientoService.actualizarAsiento(asientoNuevosDatos)

        val asientoList = asientoService.obtenerAsientos()

        assertTrue(asientoActualizado == null)

        assertTrue(asientoList!!.size == 1)
        assert(asientoList.any { it.idAsien == 123456L })
        assert(asientoList.any { it.tipoAsien == "NORMAL" })
    }

    @Test
    fun `debe actualizar un asiento y retornarlo`() {

        asientoRepository.persist(Asiento(123456, tipoAsien = "NORMAL"))

        val asientoNuevosDatos = Asiento(123456, tipoAsien = "VIP")

        val asientoActualizado = asientoService.actualizarAsiento(asientoNuevosDatos)

        val asientoList = asientoService.obtenerAsientos()

        assertTrue(asientoActualizado == asientoNuevosDatos)

        assertTrue(asientoList!!.size == 1)
        assert(asientoList.any { it.idAsien == 123456L })
        assert(asientoList.any { it.tipoAsien == "VIP" })
    }

    // ELIMINAR UN REGISTRO

    @Test
    fun `debe retornar nulo cuando no encuentre el registro que se quiere eliminar`() {

        asientoRepository.persist(Asiento(123456, tipoAsien = "NORMAL"))

        val asientoEliminar = Asiento(123457, tipoAsien = "NORMAL")

        val eliminado = asientoService.eliminarAsiento(asientoEliminar)

        val asientoList = asientoService.obtenerAsientos()

        assertTrue(!eliminado)

        assertTrue(asientoList!!.size == 1)
    }

    @Test
    fun `debe eliminar un asiento y retornar true`() {

        asientoRepository.persist(Asiento(123456, tipoAsien = "NORMAL"))

        val asientoEliminar = Asiento(123456, tipoAsien = "NORMAL")

        val eliminado = asientoService.eliminarAsiento(asientoEliminar)

        val asientoList = asientoService.obtenerAsientos()

        assertTrue(eliminado)
        assertTrue(asientoList!!.isEmpty())
    }

}