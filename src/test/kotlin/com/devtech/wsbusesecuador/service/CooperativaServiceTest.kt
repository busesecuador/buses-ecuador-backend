package com.devtech.wsbusesecuador.service

import com.devtech.wsbusesecuador.domain.Cooperativa
import com.devtech.wsbusesecuador.repository.CooperativaRepository
import io.quarkus.test.TestTransaction
import io.quarkus.test.junit.QuarkusTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import javax.inject.Inject

@QuarkusTest
@TestTransaction
class CooperativaServiceTest {

    @Inject
    lateinit var cooperativaService: CooperativaService

    @Inject
    lateinit var cooperativaRepository: CooperativaRepository

    // OBTENER TODOS LOS REGISTROS DE LA TABLA

    @Test
    fun `debe retornar vacio cuando en la tabla no hay registros`() {
        val cooperativaList = cooperativaService.obtenerCooperativas()
        assertTrue(cooperativaList.isNullOrEmpty())
    }

    @Test
    fun `debe retornar una lista de cooperativas`() {
        cooperativaRepository.persist(Cooperativa(123456, "COOP1", 2))
        cooperativaRepository.persist(Cooperativa(567890, "COOP2", 3))

        val cooperativaList = cooperativaService.obtenerCooperativas()

        assertEquals(2, cooperativaList!!.size)
        assert(cooperativaList.any { it.idCoop == 567890L })
        assert(cooperativaList.any { it.idCoop == 123456L })
    }

    // OBTENER REGISTRO POR SU ID

    @Test
    fun `debe retornar nulo cuando en la tabla no hay registros`() {
        val cooperativa = cooperativaService.obtenerCooperativaPorID(123456)
        assertTrue(cooperativa == null)
    }

    @Test
    fun `debe retornar nulo cuando en la tabla no se encuentra la cooperativa`() {
        cooperativaRepository.persist(Cooperativa(123456, "COOP1", 2))

        val cooperativa = cooperativaService.obtenerCooperativaPorID(654321)
        assertTrue(cooperativa == null)
    }

    @Test
    fun `debe retornar una cooperativa por su ID`() {
        cooperativaRepository.persist(Cooperativa(123456, "COOP1", 2))

        val cooperativa = cooperativaService.obtenerCooperativaPorID(123456)

        assertTrue(cooperativa?.idCoop == 123456L)
        assertTrue(cooperativa?.nombreCoop == "COOP1")
    }

    // OBTENER ULTIMO ID

    @Test
    fun `debe retornar nulo si no encuentra registros`() {

        val maxID = cooperativaService.obtenerMaximoID()

        assertTrue(maxID == null)
    }

    @Test
    fun `debe retornar el numero de ID maximo`() {
        cooperativaRepository.persist(Cooperativa(123455, "COOP1", 2))
        cooperativaRepository.persist(Cooperativa(123456, "COOP1", 2))
        cooperativaRepository.persist(Cooperativa(1239, "COOP1", 2))

        val maxID = cooperativaService.obtenerMaximoID()

        assertTrue(maxID == 123456L)
    }

    // INSERTAR UN NUEVO REGISTRO

    @Test
    fun `debe retornar nulo si no se pudo ingresar`() {
        val cooperativaInsertada = cooperativaService.insertarCooperativa(null)

        val cooperativaList = cooperativaService.obtenerCooperativas()

        assertTrue(cooperativaInsertada == null)

        assertTrue(cooperativaList!!.isEmpty())
    }

    @Test
    fun `debe insertar una cooperativa con el ID maximo mas uno y retornarla`() {
        cooperativaRepository.persist(Cooperativa(123456, "Coop Existente", 2))

        val cooperativaNueva = Cooperativa(1, "Nueva Coop", 2)

        val cooperativaInsertada = cooperativaService.insertarCooperativa(cooperativaNueva)

        val cooperativaList = cooperativaService.obtenerCooperativas()

        assertTrue(cooperativaInsertada?.idCoop == 123457L)

        assertTrue(cooperativaList!!.size == 2)
        assert(cooperativaList.any { it.idCoop == 123457L })
        assert(cooperativaList.any { it.idCoop == 123456L })
    }

    // ACTUALIZAR UN REGISTRO

    @Test
    fun `debe retornar nulo cuando no encuentre el registro que se quiere actualizar`() {

        cooperativaRepository.persist(Cooperativa(123456, "Nueva Coop", 2))

        val cooperativaNuevosDatos = Cooperativa(654321, "Cooperativa 1", 4)

        val cooperativaActualizada = cooperativaService.actualizarCooperativa(cooperativaNuevosDatos)

        val cooperativaList = cooperativaService.obtenerCooperativas()

        assertTrue(cooperativaActualizada == null)

        assertTrue(cooperativaList!!.size == 1)
        assert(cooperativaList.any { it.idCoop == 123456L })
        assert(cooperativaList.any { it.nombreCoop == "Nueva Coop" })
        assert(cooperativaList.any { it.cantBusesCoop == 2 })
    }

    @Test
    fun `debe actualizar una cooperativa y retornarla`() {

        cooperativaRepository.persist(Cooperativa(123456, "Nueva Coop", 2))

        val cooperativaNuevosDatos = Cooperativa(123456, "Cooperativa 1", 4)

        val cooperativaActualizada = cooperativaService.actualizarCooperativa(cooperativaNuevosDatos)

        val cooperativaList = cooperativaService.obtenerCooperativas()

        assertTrue(cooperativaActualizada?.nombreCoop == "Cooperativa 1")

        assertTrue(cooperativaList!!.size == 1)
        assert(cooperativaList.any { it.idCoop == 123456L })
        assert(cooperativaList.any { it.nombreCoop == "Cooperativa 1" })
        assert(cooperativaList.any { it.cantBusesCoop == 4 })
    }

    // ELIMINAR UN REGISTRO

    @Test
    fun `debe retornar nulo cuando no encuentre el registro que se quiere eliminar`() {

        cooperativaRepository.persist(Cooperativa(123456, "Nueva Coop", 2))

        val cooperativaEliminar = Cooperativa(654321, "Otra Coop", 2)

        val eliminado = cooperativaService.eliminarCooperativa(cooperativaEliminar)

        val cooperativaList = cooperativaService.obtenerCooperativas()

        assertTrue(!eliminado)

        assertTrue(cooperativaList!!.size == 1)
    }

    @Test
    fun `debe eliminar una cooperativa y retornar true`() {

        cooperativaRepository.persist(Cooperativa(123456, "Nueva Coop", 2))

        val cooperativaEliminar = Cooperativa(123456, "Nueva Coop", 2)

        val eliminado = cooperativaService.eliminarCooperativa(cooperativaEliminar)

        val cooperativaList = cooperativaService.obtenerCooperativas()

        assertTrue(eliminado)
        assertTrue(cooperativaList!!.isEmpty())
    }


}