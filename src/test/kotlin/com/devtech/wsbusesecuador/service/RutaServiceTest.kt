package com.devtech.wsbusesecuador.service

import com.devtech.wsbusesecuador.domain.Ruta
import com.devtech.wsbusesecuador.repository.RutaRepository
import io.quarkus.test.TestTransaction
import io.quarkus.test.junit.QuarkusTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import javax.inject.Inject

@QuarkusTest
@TestTransaction
class RutaServiceTest {

    @Inject
    lateinit var rutaService: RutaService

    @Inject
    lateinit var rutaRepository: RutaRepository

    // OBTENER TODOS LOS REGISTROS DE LA TABLA

    @Test
    fun `debe retornar vacio cuando en la tabla no hay registros`() {
        val rutaList = rutaService.obtenerRutas()
        assertTrue(rutaList.isNullOrEmpty())
    }

    @Test
    fun `debe retornar una lista de rutas`() {
        rutaRepository.persist(Ruta(123456, precioRuta = 10.99f))
        rutaRepository.persist(Ruta(567890, precioRuta = 9.99f))

        val rutaList = rutaService.obtenerRutas()

        assertEquals(2, rutaList!!.size)
        assert(rutaList.any { it.idRuta == 567890L })
        assert(rutaList.any { it.idRuta == 123456L })
    }

    // OBTENER REGISTRO POR SU ID

    @Test
    fun `debe retornar nulo cuando en la tabla no hay registros`() {
        val ruta = rutaService.obtenerRutaPorID(123456)
        assertTrue(ruta == null)
    }

    @Test
    fun `debe retornar nulo cuando en la tabla no se encuentra la ruta`() {
        rutaRepository.persist(Ruta(123456, precioRuta = 10.99f))

        val ruta = rutaService.obtenerRutaPorID(654321)
        assertTrue(ruta == null)
    }

    @Test
    fun `debe retornar una ruta por su ID`() {
        rutaRepository.persist(Ruta(123456, precioRuta = 10.99f))

        val ruta = rutaService.obtenerRutaPorID(123456)

        assertTrue(ruta?.idRuta == 123456L)
        assertTrue(ruta?.precioRuta == 10.99f)
    }

    // OBTENER ULTIMO ID

    @Test
    fun `debe retornar nulo si no encuentra registros`() {

        val maxID = rutaService.obtenerMaximoID()

        assertTrue(maxID == null)
    }

    @Test
    fun `debe retornar el numero de ID maximo`() {
        rutaRepository.persist(Ruta(123455, precioRuta = 8.99f))
        rutaRepository.persist(Ruta(123456, precioRuta = 10.99f))
        rutaRepository.persist(Ruta(1239, precioRuta = 6.99f))

        val maxID = rutaService.obtenerMaximoID()

        assertTrue(maxID == 123456L)
    }

    // INSERTAR UN NUEVO REGISTRO

    @Test
    fun `debe retornar nulo si no se pudo ingresar`() {
        val rutaInsertada = rutaService.insertarRuta(null)

        val rutaList = rutaService.obtenerRutas()

        assertTrue(rutaInsertada == null)

        assertTrue(rutaList!!.isEmpty())
    }

    @Test
    fun `debe insertar una ruta con el ID maximo mas uno y retornarla`() {
        rutaRepository.persist(Ruta(123456, precioRuta = 10.99f))

        val rutaNueva = Ruta(1, precioRuta = 11.99f)

        val rutaInsertada = rutaService.insertarRuta(rutaNueva)

        val rutaList = rutaService.obtenerRutas()

        assertTrue(rutaInsertada?.idRuta == 123457L)

        assertTrue(rutaList!!.size == 2)
        assert(rutaList.any { it.idRuta == 123457L })
        assert(rutaList.any { it.idRuta == 123456L })
    }

    // ACTUALIZAR UN REGISTRO

    @Test
    fun `debe retornar nulo cuando no encuentre el registro que se quiere actualizar`() {

        rutaRepository.persist(Ruta(123456, precioRuta = 10.99f))

        val rutaNuevosDatos = Ruta(654321, precioRuta = 11.99f)

        val rutaActualizada = rutaService.actualizarRuta(rutaNuevosDatos)

        val rutaList = rutaService.obtenerRutas()

        assertTrue(rutaActualizada == null)

        assertTrue(rutaList!!.size == 1)
        assert(rutaList.any { it.idRuta == 123456L })
    }

    @Test
    fun `debe actualizar una ruta y retornarla`() {

        rutaRepository.persist(Ruta(123456, precioRuta = 10.99f))

        val rutaNuevosDatos = Ruta(123456, precioRuta = 11.99f)

        val rutaActualizada = rutaService.actualizarRuta(rutaNuevosDatos)

        val rutaList = rutaService.obtenerRutas()

        assertTrue(rutaActualizada?.precioRuta == 11.99f)

        assertTrue(rutaList!!.size == 1)
        assert(rutaList.any { it.idRuta == 123456L })
        assert(rutaList.any { it.precioRuta == 11.99f })
    }

    // ELIMINAR UN REGISTRO

    @Test
    fun `debe retornar nulo cuando no encuentre el registro que se quiere eliminar`() {

        rutaRepository.persist(Ruta(123456, precioRuta = 10.99f))

        val rutaEliminar = Ruta(654321, precioRuta = 10.99f)

        val eliminado = rutaService.eliminarRuta(rutaEliminar)

        val rutaList = rutaService.obtenerRutas()

        assertTrue(!eliminado)

        assertTrue(rutaList!!.size == 1)
    }

    @Test
    fun `debe eliminar una ruta y retornar true`() {

        rutaRepository.persist(Ruta(123456, precioRuta = 10.99f))

        val rutaEliminar = Ruta(123456, precioRuta = 10.99f)

        val eliminado = rutaService.eliminarRuta(rutaEliminar)

        val rutaList = rutaService.obtenerRutas()

        assertTrue(eliminado)
        assertTrue(rutaList!!.isEmpty())
    }

    // SERVICIOS ADICIONALES

    @Test
    fun `debe retornar una lista de rutas por ciudad de inicio`() {
        rutaRepository.persist(Ruta(123456, precioRuta = 10.99f, ciudadIniRuta = "QUITO"))
        rutaRepository.persist(Ruta(789012, precioRuta = 10.99f, ciudadIniRuta = "QUITO"))
        rutaRepository.persist(Ruta(987654, precioRuta = 10.99f, ciudadIniRuta = "AMBATO"))

        val rutas = rutaService.obtenerRutasPorCiudadInicio("QUITO  ")

        assertEquals(2, rutas.size)
    }

}