package com.devtech.wsbusesecuador.service

import com.devtech.wsbusesecuador.domain.Persona
import com.devtech.wsbusesecuador.repository.PersonaRepository
import io.quarkus.test.TestTransaction
import io.quarkus.test.junit.QuarkusTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import javax.inject.Inject

@QuarkusTest
@TestTransaction
class PersonaServiceTest {

    @Inject
    lateinit var personaService: PersonaService

    @Inject
    lateinit var personaRepository: PersonaRepository

    // OBTENER TODOS LOS REGISTROS DE LA TABLA

    @Test
    fun `debe retornar vacio cuando en la tabla no hay registros`() {
        val personaList = personaService.obtenerPersonas()
        assertTrue(personaList.isNullOrEmpty())
    }

    @Test
    fun `debe retornar una lista de personas`() {
        personaRepository.persist(Persona(123456, "Manuel Naranjo", "1897645309"))
        personaRepository.persist(Persona(654321, "Manuel Ibarra", "1897645310"))

        val personaList = personaService.obtenerPersonas()

        assertEquals(2, personaList!!.size)
        assert(personaList.any { it.idPers == 654321L })
        assert(personaList.any { it.idPers == 123456L })
    }

    // OBTENER REGISTRO POR SU ID

    @Test
    fun `debe retornar nulo cuando en la tabla no hay registros`() {
        val persona = personaService.obtenerPersonaPorID(123456)
        assertTrue(persona == null)
    }

    @Test
    fun `debe retornar nulo cuando en la tabla no se encuentra la persona`() {
        personaRepository.persist(Persona(123456, "Manuel Naranjo", "1897645309"))

        val persona = personaService.obtenerPersonaPorID(654321)
        assertTrue(persona == null)
    }

    @Test
    fun `debe retornar una persona por su ID`() {
        personaRepository.persist(Persona(123456, "Manuel Naranjo", "1897645309"))

        val persona = personaService.obtenerPersonaPorID(123456)

        assertTrue(persona?.idPers == 123456L)
        assertTrue(persona?.nombrePers == "Manuel Naranjo")
    }

    // OBTENER ULTIMO ID

    @Test
    fun `debe retornar nulo si no encuentra registros`() {

        val maxID = personaService.obtenerMaximoID()

        assertTrue(maxID == null)
    }

    @Test
    fun `debe retornar el numero de ID maximo`() {
        personaRepository.persist(Persona(123455, "Manuel Naranjo", "1897645309"))
        personaRepository.persist(Persona(123456, "Manuel Perez", "1897645311"))
        personaRepository.persist(Persona(1239, "Manuel Hernandez", "1897645312"))

        val maxID = personaService.obtenerMaximoID()

        assertTrue(maxID == 123456L)
    }

    // INSERTAR UN NUEVO REGISTRO

    @Test
    fun `debe retornar nulo si no se pudo ingresar`() {
        val personaInsertada = personaService.insertarPersona(null)

        val personaList = personaService.obtenerPersonas()

        assertTrue(personaInsertada == null)

        assertTrue(personaList!!.isEmpty())
    }

    @Test
    fun `debe insertar una persona con el ID maximo mas uno y retornarla`() {
        personaRepository.persist(Persona(123456, "Manuel Naranjo", "1897645309"))

        val personaNueva = Persona(1, "Manuel Jara", "1897645312")

        val personaInsertada = personaService.insertarPersona(personaNueva)

        val personaList = personaService.obtenerPersonas()

        assertTrue(personaInsertada?.idPers == 123457L)

        assertTrue(personaList!!.size == 2)
        assert(personaList.any { it.idPers == 123457L })
        assert(personaList.any { it.idPers == 123456L })
    }

    // ACTUALIZAR UN REGISTRO

    @Test
    fun `debe retornar nulo cuando no encuentre el registro que se quiere actualizar`() {

        personaRepository.persist(Persona(123456, "Manuel Naranjo", "1897645309"))

        val personaNuevosDatos = Persona(654321, "Manuel Jose Naranjo", "1897645310")

        val personaActualizada = personaService.actualizarPersona(personaNuevosDatos)

        val personaList = personaService.obtenerPersonas()

        assertTrue(personaActualizada == null)

        assertTrue(personaList!!.size == 1)
        assert(personaList.any { it.idPers == 123456L })
        assert(personaList.any { it.nombrePers == "Manuel Naranjo" })
        assert(personaList.any { it.cedPers == "1897645309" })
    }

    @Test
    fun `debe actualizar una persona y retornarlo`() {

        personaRepository.persist(Persona(123456, "Manuel Naranjo", "1897645309"))

        val personaNuevosDatos = Persona(123456, "Manuel Jose Naranjo", "1897645310")

        val personaActualizada = personaService.actualizarPersona(personaNuevosDatos)

        val personaList = personaService.obtenerPersonas()

        assertTrue(personaActualizada == personaNuevosDatos)

        assertTrue(personaList!!.size == 1)
        assert(personaList.any { it.idPers == 123456L })
        assert(personaList.any { it.nombrePers == "Manuel Jose Naranjo" })
        assert(personaList.any { it.cedPers == "1897645310" })
    }

    // ELIMINAR UN REGISTRO

    @Test
    fun `debe retornar nulo cuando no encuentre el registro que se quiere eliminar`() {

        personaRepository.persist(Persona(123456, "Manuel Naranjo", "1897645309"))

        val personaEliminar = Persona(654321, "Manuel Jose Naranjo", "1897645310")

        val eliminado = personaService.eliminarPersona(personaEliminar)

        val personaList = personaService.obtenerPersonas()

        assertTrue(!eliminado)

        assertTrue(personaList!!.size == 1)
    }

    @Test
    fun `debe eliminar una persona y retornar true`() {

        personaRepository.persist(Persona(123456, "Manuel Naranjo", "1897645309"))

        val personaEliminar = Persona(123456, "Manuel Naranjo", "1897645309")

        val eliminado = personaService.eliminarPersona(personaEliminar)

        val personaList = personaService.obtenerPersonas()

        assertTrue(eliminado)
        assertTrue(personaList!!.isEmpty())
    }

    // LOGEO DE USUARIO

    @Test
    fun `debe retornar nulo cuando no encuentre el username`() {
        personaRepository.persist(Persona(123456, "Manuel Naranjo", "1897645309", "mnaranjo", "pass12345"))

        val personaLogeada = personaService.login("mmnaranj", "pass12345")

        assertTrue(personaLogeada == null)
    }

    @Test
    fun `debe retornar nulo cuando el password sea incorrecto`() {
        personaRepository.persist(Persona(123456, "Manuel Naranjo", "1897645309", "mnaranjo", "pass12345"))

        val personaLogeada = personaService.login("mnaranjo", "pass123")

        assertTrue(personaLogeada == null)
    }

    @Test
    fun `debe retornar a la persona si su usuario y contraseña son correctos`() {
        personaRepository.persist(Persona(123456, "Manuel Naranjo", "1897645309", "mnaranjo", "pass12345"))

        val personaLogeada = personaService.login("mnaranjo", "pass12345")

        assertEquals(123456, personaLogeada?.idPers)
        assertEquals("Manuel Naranjo", personaLogeada?.nombrePers)
    }

}