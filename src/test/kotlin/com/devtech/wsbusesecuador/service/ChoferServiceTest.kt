package com.devtech.wsbusesecuador.service

import com.devtech.wsbusesecuador.domain.Chofer
import com.devtech.wsbusesecuador.repository.ChoferRepository
import io.quarkus.test.TestTransaction
import io.quarkus.test.junit.QuarkusTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import javax.inject.Inject

@QuarkusTest
@TestTransaction
class ChoferServiceTest {

    @Inject
    lateinit var choferService: ChoferService

    @Inject
    lateinit var choferRepository: ChoferRepository

    // OBTENER TODOS LOS REGISTROS DE LA TABLA

    @Test
    fun `debe retornar vacio cuando en la tabla no hay registros`() {
        val choferList = choferService.obtenerChoferes()
        assertTrue(choferList.isNullOrEmpty())
    }

    @Test
    fun `debe retornar una lista de choferes`() {
        choferRepository.persist(Chofer(123456, "Manuel Naranjo", "1897645309"))
        choferRepository.persist(Chofer(654321, "Manuel Ibarra", "1897645310"))

        val choferList = choferService.obtenerChoferes()

        assertEquals(2, choferList!!.size)
        assert(choferList.any { it.idChof == 654321L })
        assert(choferList.any { it.idChof == 123456L })
    }

    // OBTENER REGISTRO POR SU ID

    @Test
    fun `debe retornar nulo cuando en la tabla no hay registros`() {
        val chofer = choferService.obtenerChoferPorID(123456)
        assertTrue(chofer == null)
    }

    @Test
    fun `debe retornar nulo cuando en la tabla no se encuentra el chofer`() {
        choferRepository.persist(Chofer(123456, "Manuel Naranjo", "1897645309"))

        val chofer = choferService.obtenerChoferPorID(654321)
        assertTrue(chofer == null)
    }

    @Test
    fun `debe retornar un chofer por su ID`() {
        choferRepository.persist(Chofer(123456, "Manuel Naranjo", "1897645309"))

        val chofer = choferService.obtenerChoferPorID(123456)

        assertTrue(chofer?.idChof == 123456L)
        assertTrue(chofer?.nombreChof == "Manuel Naranjo")
    }

    // OBTENER ULTIMO ID

    @Test
    fun `debe retornar nulo si no encuentra registros`() {

        val maxID = choferService.obtenerMaximoID()

        assertTrue(maxID == null)
    }

    @Test
    fun `debe retornar el numero de ID maximo`() {
        choferRepository.persist(Chofer(123455, "Manuel Naranjo", "1897645309"))
        choferRepository.persist(Chofer(123456, "Manuel Perez", "1897645311"))
        choferRepository.persist(Chofer(1239, "Manuel Hernandez", "1897645312"))

        val maxID = choferService.obtenerMaximoID()

        assertTrue(maxID == 123456L)
    }

    // INSERTAR UN NUEVO REGISTRO

    @Test
    fun `debe retornar nulo si no se pudo ingresar`() {
        val choferInsertado = choferService.insertarChofer(null)

        val choferList = choferService.obtenerChoferes()

        assertTrue(choferInsertado == null)

        assertTrue(choferList!!.isEmpty())
    }

    @Test
    fun `debe insertar un chofer con el ID maximo mas uno y retornarla`() {
        choferRepository.persist(Chofer(123456, "Manuel Naranjo", "1897645309"))

        val choferNuevo = Chofer(1, "Manuel Jara", "1897645312")

        val choferInsertado = choferService.insertarChofer(choferNuevo)

        val choferList = choferService.obtenerChoferes()

        assertTrue(choferInsertado?.idChof == 123457L)

        assertTrue(choferList!!.size == 2)
        assert(choferList.any { it.idChof == 123457L })
        assert(choferList.any { it.idChof == 123456L })
    }

    // ACTUALIZAR UN REGISTRO

    @Test
    fun `debe retornar nulo cuando no encuentre el registro que se quiere actualizar`() {

        choferRepository.persist(Chofer(123456, "Manuel Naranjo", "1897645309"))

        val choferNuevosDatos = Chofer(654321, "Manuel Jose Naranjo", "1897645310")

        val choferActualizado = choferService.actualizarChofer(choferNuevosDatos)

        val choferList = choferService.obtenerChoferes()

        assertTrue(choferActualizado == null)

        assertTrue(choferList!!.size == 1)
        assert(choferList.any { it.idChof == 123456L })
        assert(choferList.any { it.nombreChof == "Manuel Naranjo" })
        assert(choferList.any { it.cedChof == "1897645309" })
    }

    @Test
    fun `debe actualizar un chofer y retornarlo`() {

        choferRepository.persist(Chofer(123456, "Manuel Naranjo", "1897645309"))

        val choferNuevosDatos = Chofer(123456, "Manuel Jose Naranjo", "1897645310")

        val choferActualizado = choferService.actualizarChofer(choferNuevosDatos)

        val choferList = choferService.obtenerChoferes()

        assertTrue(choferActualizado == choferNuevosDatos)

        assertTrue(choferList!!.size == 1)
        assert(choferList.any { it.idChof == 123456L })
        assert(choferList.any { it.nombreChof == "Manuel Jose Naranjo" })
        assert(choferList.any { it.cedChof == "1897645310" })
    }

    // ELIMINAR UN REGISTRO

    @Test
    fun `debe retornar nulo cuando no encuentre el registro que se quiere eliminar`() {

        choferRepository.persist(Chofer(123456, "Manuel Naranjo", "1897645309"))

        val choferEliminar = Chofer(654321, "Manuel Jose Naranjo", "1897645310")

        val eliminado = choferService.eliminarChofer(choferEliminar)

        val choferList = choferService.obtenerChoferes()

        assertTrue(!eliminado)

        assertTrue(choferList!!.size == 1)
    }

    @Test
    fun `debe eliminar un chofer y retornar true`() {

        choferRepository.persist(Chofer(123456, "Manuel Naranjo", "1897645309"))

        val choferEliminar = Chofer(123456, "Manuel Naranjo", "1897645309")

        val eliminado = choferService.eliminarChofer(choferEliminar)

        val choferList = choferService.obtenerChoferes()

        assertTrue(eliminado)
        assertTrue(choferList!!.isEmpty())
    }

}