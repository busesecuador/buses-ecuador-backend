package com.devtech.wsbusesecuador.service

import com.devtech.wsbusesecuador.domain.Frecuencia
import com.devtech.wsbusesecuador.repository.FrecuenciaRepository
import io.quarkus.test.TestTransaction
import io.quarkus.test.junit.QuarkusTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import javax.inject.Inject

@QuarkusTest
@TestTransaction
class FrecuenciaServiceTest {

    @Inject
    lateinit var frecuenciaService: FrecuenciaService

    @Inject
    lateinit var frecuenciaRepository: FrecuenciaRepository

    // OBTENER TODOS LOS REGISTROS DE LA TABLA

    @Test
    fun `debe retornar vacio cuando en la tabla no hay registros`() {
        val frecuenciaList = frecuenciaService.obtenerFrecuencias()
        assertTrue(frecuenciaList.isNullOrEmpty())
    }

    @Test
    fun `debe retornar una lista de frecuencias`() {
        frecuenciaRepository.persist(Frecuencia(123456, habilitadaFrec = "Y"))
        frecuenciaRepository.persist(Frecuencia(567890, habilitadaFrec = "N"))

        val frecuenciaList = frecuenciaService.obtenerFrecuencias()

        assertEquals(2, frecuenciaList!!.size)
        assert(frecuenciaList.any { it.idFrec == 567890L })
        assert(frecuenciaList.any { it.idFrec == 123456L })
    }

    // OBTENER REGISTRO POR SU ID

    @Test
    fun `debe retornar nulo cuando en la tabla no hay registros`() {
        val frecuencia = frecuenciaService.obtenerFrecuenciaPorID(123456)
        assertTrue(frecuencia == null)
    }

    @Test
    fun `debe retornar nulo cuando en la tabla no se encuentra la frecuencia`() {
        frecuenciaRepository.persist(Frecuencia(123456, habilitadaFrec = "Y"))

        val frecuencia = frecuenciaService.obtenerFrecuenciaPorID(654321)
        assertTrue(frecuencia == null)
    }

    @Test
    fun `debe retornar una frecuencia por su ID`() {
        frecuenciaRepository.persist(Frecuencia(123456, habilitadaFrec = "Y"))

        val frecuencia = frecuenciaService.obtenerFrecuenciaPorID(123456)

        assertTrue(frecuencia?.idFrec == 123456L)
        assertTrue(frecuencia?.habilitadaFrec == "Y")
    }

    // OBTENER ULTIMO ID

    @Test
    fun `debe retornar nulo si no encuentra registros`() {

        val maxID = frecuenciaService.obtenerMaximoID()

        assertTrue(maxID == null)
    }

    @Test
    fun `debe retornar el numero de ID maximo`() {
        frecuenciaRepository.persist(Frecuencia(123455, habilitadaFrec = "N"))
        frecuenciaRepository.persist(Frecuencia(123456, habilitadaFrec = "Y"))
        frecuenciaRepository.persist(Frecuencia(1239, habilitadaFrec = "N"))

        val maxID = frecuenciaService.obtenerMaximoID()

        assertTrue(maxID == 123456L)
    }

    // INSERTAR UN NUEVO REGISTRO

    @Test
    fun `debe retornar nulo si no se pudo ingresar`() {
        val frecuenciaInsertada = frecuenciaService.insertarFrecuencia(null)

        val frecuenciaList = frecuenciaService.obtenerFrecuencias()

        assertTrue(frecuenciaInsertada == null)

        assertTrue(frecuenciaList!!.isEmpty())
    }

    @Test
    fun `debe insertar una frecuencia con el ID maximo mas uno y retornarla`() {
        frecuenciaRepository.persist(Frecuencia(123456, habilitadaFrec = "N"))

        val frecuenciaNueva = Frecuencia(1, habilitadaFrec = "N")

        val frecuenciaInsertada = frecuenciaService.insertarFrecuencia(frecuenciaNueva)

        val frecuenciaList = frecuenciaService.obtenerFrecuencias()

        assertTrue(frecuenciaInsertada?.idFrec == 123457L)

        assertTrue(frecuenciaList!!.size == 2)
        assert(frecuenciaList.any { it.idFrec == 123457L })
        assert(frecuenciaList.any { it.idFrec == 123456L })
    }

    // ACTUALIZAR UN REGISTRO

    @Test
    fun `debe retornar nulo cuando no encuentre el registro que se quiere actualizar`() {

        frecuenciaRepository.persist(Frecuencia(123456, habilitadaFrec = "Y"))

        val frecuenciaNuevosDatos = Frecuencia(654321, habilitadaFrec = "N")

        val frecuenciaActualizada = frecuenciaService.actualizarFrecuencia(frecuenciaNuevosDatos)

        val frecuenciaList = frecuenciaService.obtenerFrecuencias()

        assertTrue(frecuenciaActualizada == null)

        assertTrue(frecuenciaList!!.size == 1)
        assert(frecuenciaList.any { it.idFrec == 123456L })
    }

    @Test
    fun `debe actualizar una frecuencia y retornarla`() {

        frecuenciaRepository.persist(Frecuencia(123456, habilitadaFrec = "Y"))

        val frecuenciaNuevosDatos = Frecuencia(123456, habilitadaFrec = "N")

        val frecuenciaActualizada = frecuenciaService.actualizarFrecuencia(frecuenciaNuevosDatos)

        val frecuenciaList = frecuenciaService.obtenerFrecuencias()

        assertTrue(frecuenciaActualizada?.habilitadaFrec == "N")

        assertTrue(frecuenciaList!!.size == 1)
        assert(frecuenciaList.any { it.idFrec == 123456L })
        assert(frecuenciaList.any { it.habilitadaFrec == "N" })
    }

    // ELIMINAR UN REGISTRO

    @Test
    fun `debe retornar nulo cuando no encuentre el registro que se quiere eliminar`() {

        frecuenciaRepository.persist(Frecuencia(123456, habilitadaFrec ="Y"))

        val frecuenciaEliminar = Frecuencia(654321, habilitadaFrec = "N")

        val eliminado = frecuenciaService.eliminarFrecuencia(frecuenciaEliminar)

        val frecuenciaList = frecuenciaService.obtenerFrecuencias()

        assertTrue(!eliminado)

        assertTrue(frecuenciaList!!.size == 1)
    }

    @Test
    fun `debe eliminar una frecuencia y retornar true`() {

        frecuenciaRepository.persist(Frecuencia(123456, habilitadaFrec ="Y"))

        val frecuenciaEliminar = Frecuencia(123456, habilitadaFrec = "N")

        val eliminado = frecuenciaService.eliminarFrecuencia(frecuenciaEliminar)

        val frecuenciaList = frecuenciaService.obtenerFrecuencias()

        assertTrue(eliminado)
        assertTrue(frecuenciaList!!.isEmpty())
    }

}