package com.devtech.wsbusesecuador.service

import com.devtech.wsbusesecuador.domain.Bus
import com.devtech.wsbusesecuador.repository.BusRepository
import io.quarkus.test.TestTransaction
import io.quarkus.test.junit.QuarkusTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import javax.inject.Inject

@QuarkusTest
@TestTransaction
class BusServiceTest {

    @Inject
    lateinit var busService: BusService

    @Inject
    lateinit var busRepository: BusRepository

    // OBTENER TODOS LOS REGISTROS DE LA TABLA

    @Test
    fun `debe retornar vacio cuando en la tabla no hay registros`() {
        val busList = busService.obtenerBuses()
        assertTrue(busList.isNullOrEmpty())
    }

    @Test
    fun `debe retornar una lista de buses`() {
        busRepository.persist(Bus(123456, numBus = 980))
        busRepository.persist(Bus(654321, numBus = 981))

        val busList = busService.obtenerBuses()

        assertEquals(2, busList!!.size)
        assert(busList.any { it.idBus == 654321L })
        assert(busList.any { it.idBus == 123456L })
    }

    // OBTENER REGISTRO POR SU ID

    @Test
    fun `debe retornar nulo cuando en la tabla no hay registros`() {
        val bus = busService.obtenerBusPorID(123456)
        assertTrue(bus == null)
    }

    @Test
    fun `debe retornar nulo cuando en la tabla no se encuentra el bus`() {
        busRepository.persist(Bus(123456, numBus = 980))

        val bus = busService.obtenerBusPorID(654321)
        assertTrue(bus == null)
    }

    @Test
    fun `debe retornar un bus por su ID`() {
        busRepository.persist(Bus(123456, numBus = 980))

        val bus = busService.obtenerBusPorID(123456)

        assertTrue(bus?.idBus == 123456L)
        assertTrue(bus?.numBus == 980L)
    }

    // OBTENER ULTIMO ID

    @Test
    fun `debe retornar nulo si no encuentra registros`() {

        val maxID = busService.obtenerMaximoID()

        assertTrue(maxID == null)
    }

    @Test
    fun `debe retornar el numero de ID maximo`() {
        busRepository.persist(Bus(123455, numBus = 980))
        busRepository.persist(Bus(123456, numBus = 981))
        busRepository.persist(Bus(1239, numBus = 985))

        val maxID = busService.obtenerMaximoID()

        assertTrue(maxID == 123456L)
    }

    // INSERTAR UN NUEVO REGISTRO

    @Test
    fun `debe retornar nulo si no se pudo ingresar`() {
        val busInsertado = busService.insertarBus(null)

        val busList = busService.obtenerBuses()

        assertTrue(busInsertado == null)

        assertTrue(busList!!.isEmpty())
    }

    @Test
    fun `debe insertar un bus con el ID maximo mas uno y retornarla`() {
        busRepository.persist(Bus(123456, numBus = 980))

        val busNuevo = Bus(1, numBus = 980)

        val busInsertado = busService.insertarBus(busNuevo)

        val busList = busService.obtenerBuses()

        assertTrue(busInsertado?.idBus == 123457L)

        assertTrue(busList!!.size == 2)
        assert(busList.any { it.idBus == 123457L })
        assert(busList.any { it.idBus == 123456L })
    }

    // ACTUALIZAR UN REGISTRO

    @Test
    fun `debe retornar nulo cuando no encuentre el registro que se quiere actualizar`() {

        busRepository.persist(Bus(123456, numBus = 980))

        val busNuevosDatos = Bus(123457, numBus = 981)

        val busActualizado = busService.actualizarBus(busNuevosDatos)

        val busList = busService.obtenerBuses()

        assertTrue(busActualizado == null)

        assertTrue(busList!!.size == 1)
        assert(busList.any { it.idBus == 123456L })
        assert(busList.any { it.numBus == 980L })
    }

    @Test
    fun `debe actualizar un bus y retornarlo`() {

        busRepository.persist(Bus(123456, numBus = 980))

        val busNuevosDatos = Bus(123456, numBus = 981)

        val busActualizado = busService.actualizarBus(busNuevosDatos)

        val busList = busService.obtenerBuses()

        assertTrue(busActualizado == busNuevosDatos)

        assertTrue(busList!!.size == 1)
        assert(busList.any { it.idBus == 123456L })
        assert(busList.any { it.numBus == 981L })
    }

    // ELIMINAR UN REGISTRO

    @Test
    fun `debe retornar nulo cuando no encuentre el registro que se quiere eliminar`() {

        busRepository.persist(Bus(123456, numBus = 980))

        val busEliminar = Bus(123457, numBus = 980)

        val eliminado = busService.eliminarBus(busEliminar)

        val busList = busService.obtenerBuses()

        assertTrue(!eliminado)

        assertTrue(busList!!.size == 1)
    }

    @Test
    fun `debe eliminar un bus y retornar true`() {

        busRepository.persist(Bus(123456, numBus = 980))

        val busEliminar = Bus(123456, numBus = 980)

        val eliminado = busService.eliminarBus(busEliminar)

        val busList = busService.obtenerBuses()

        assertTrue(eliminado)
        assertTrue(busList!!.isEmpty())
    }

}