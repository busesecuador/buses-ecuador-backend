package com.devtech.wsbusesecuador.service

import com.devtech.wsbusesecuador.domain.Boleto
import com.devtech.wsbusesecuador.repository.BoletoRepository
import io.quarkus.test.TestTransaction
import io.quarkus.test.junit.QuarkusTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import javax.inject.Inject

@QuarkusTest
@TestTransaction
class BoletoServiceTest {

    @Inject
    lateinit var boletoService: BoletoService

    @Inject
    lateinit var boletoRepository: BoletoRepository

    // OBTENER TODOS LOS REGISTROS DE LA TABLA

    @Test
    fun `debe retornar vacio cuando en la tabla no hay registros`() {
        val boletoList = boletoService.obtenerBoletos()
        assertTrue(boletoList.isNullOrEmpty())
    }

    @Test
    fun `debe retornar una lista de boletos`() {
        boletoRepository.persist(Boleto(123456, comprobPagoBol  = "987654"))
        boletoRepository.persist(Boleto(654321, comprobPagoBol  = "456789"))

        val boletoList = boletoService.obtenerBoletos()

        assertEquals(2, boletoList!!.size)
        assert(boletoList.any { it.idBol == 654321L })
        assert(boletoList.any { it.idBol == 123456L })
    }

    // OBTENER REGISTRO POR SU ID

    @Test
    fun `debe retornar nulo cuando en la tabla no hay registros`() {
        val boleto = boletoService.obtenerBoletoPorID(123456)
        assertTrue(boleto == null)
    }

    @Test
    fun `debe retornar nulo cuando en la tabla no se encuentra el boleto`() {
        boletoRepository.persist(Boleto(123456, comprobPagoBol  = "987654"))

        val boleto = boletoService.obtenerBoletoPorID(654321)
        assertTrue(boleto == null)
    }

    @Test
    fun `debe retornar un boleto por su ID`() {
        boletoRepository.persist(Boleto(123456, comprobPagoBol  = "987654"))

        val boleto = boletoService.obtenerBoletoPorID(123456)

        assertTrue(boleto?.idBol == 123456L)
        assertTrue(boleto?.comprobPagoBol == "987654")
    }

    // OBTENER ULTIMO ID

    @Test
    fun `debe retornar nulo si no encuentra registros`() {

        val maxID = boletoService.obtenerMaximoID()

        assertTrue(maxID == null)
    }

    @Test
    fun `debe retornar el numero de ID maximo`() {
        boletoRepository.persist(Boleto(123456, comprobPagoBol  = "987654"))
        boletoRepository.persist(Boleto(21323, comprobPagoBol  = "21323"))
        boletoRepository.persist(Boleto(1456, comprobPagoBol  = "1456"))

        val maxID = boletoService.obtenerMaximoID()

        assertTrue(maxID == 123456L)
    }

    // INSERTAR UN NUEVO REGISTRO

    @Test
    fun `debe retornar nulo si no se pudo ingresar`() {
        val boletoInsertado = boletoService.insertarBoleto(null)

        val boletoList = boletoService.obtenerBoletos()

        assertTrue(boletoInsertado == null)

        assertTrue(boletoList!!.isEmpty())
    }

    @Test
    fun `debe insertar un boleto con el ID maximo mas uno y retornarla`() {
        boletoRepository.persist(Boleto(123456, comprobPagoBol  = "987654"))

        val boletoNuevo = Boleto(1, comprobPagoBol  = "9876543")

        val boletoInsertado = boletoService.insertarBoleto(boletoNuevo)

        val boletoList = boletoService.obtenerBoletos()

        assertTrue(boletoInsertado?.idBol == 123457L)

        assertTrue(boletoList!!.size == 2)
        assert(boletoList.any { it.idBol == 123457L })
        assert(boletoList.any { it.idBol == 123456L })
    }

    // ACTUALIZAR UN REGISTRO

    @Test
    fun `debe retornar nulo cuando no encuentre el registro que se quiere actualizar`() {

        boletoRepository.persist(Boleto(123456, comprobPagoBol  = "987654"))

        val boletoNuevosDatos = Boleto(123457, comprobPagoBol  = "9876543")

        val boletoActualizado = boletoService.actualizarBoleto(boletoNuevosDatos)

        val boletoList = boletoService.obtenerBoletos()

        assertTrue(boletoActualizado == null)

        assertTrue(boletoList!!.size == 1)
        assert(boletoList.any { it.idBol == 123456L })
        assert(boletoList.any { it.comprobPagoBol== "987654" })
    }

    @Test
    fun `debe actualizar un boleto y retornarlo`() {

        boletoRepository.persist(Boleto(123456, comprobPagoBol  = "987654"))

        val boletoNuevosDatos = Boleto(123456, comprobPagoBol  = "9876543")

        val boletoActualizado = boletoService.actualizarBoleto(boletoNuevosDatos)

        val boletoList = boletoService.obtenerBoletos()

        assertTrue(boletoActualizado == boletoNuevosDatos)

        assertTrue(boletoList!!.size == 1)
        assert(boletoList.any { it.idBol == 123456L })
        assert(boletoList.any { it.comprobPagoBol == "9876543" })
    }

    // ELIMINAR UN REGISTRO

    @Test
    fun `debe retornar nulo cuando no encuentre el registro que se quiere eliminar`() {

        boletoRepository.persist(Boleto(123456, comprobPagoBol  = "987654"))

        val boletoEliminar = Boleto(123457, comprobPagoBol  = "987654")

        val eliminado = boletoService.eliminarBoleto(boletoEliminar)

        val boletoList = boletoService.obtenerBoletos()

        assertTrue(!eliminado)

        assertTrue(boletoList!!.size == 1)
    }

    @Test
    fun `debe eliminar un boleto y retornar true`() {

        boletoRepository.persist(Boleto(123456, comprobPagoBol  = "987654"))

        val boletoEliminar = Boleto(123456, comprobPagoBol  = "987654")

        val eliminado = boletoService.eliminarBoleto(boletoEliminar)

        val boletoList = boletoService.obtenerBoletos()

        assertTrue(eliminado)
        assertTrue(boletoList!!.isEmpty())
    }

}