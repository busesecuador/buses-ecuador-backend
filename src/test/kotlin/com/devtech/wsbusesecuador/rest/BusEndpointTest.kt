package com.devtech.wsbusesecuador.rest

import com.devtech.wsbusesecuador.domain.Bus
import com.devtech.wsbusesecuador.service.BusService
import io.quarkus.test.TestTransaction
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.mockito.InjectMock
import io.restassured.RestAssured.given
import io.restassured.http.ContentType
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.hasItem
import org.hamcrest.CoreMatchers.`is`
import org.junit.jupiter.api.Test
import org.mockito.Mockito

@QuarkusTest
@TestTransaction
class BusEndpointTest {

    @InjectMock
    lateinit var busServiceMock: BusService

    // OBTENER TODOS LOS REGISTROS DE LA TABLA

    @Test
    fun `debe retornar nulo cuando no existan buses`() {
        Mockito.`when`(busServiceMock.obtenerBuses())
            .thenReturn(null)
        given()
            .`when`().get("/buses/obtener-buses")
            .then()
            .statusCode(204)
    }

    @Test
    fun `debe retornar una lista de buses`() {

        val busesList = ArrayList<Bus>()
        val bus1 = Bus(123456, numBus = 980)
        val bus2 = Bus(456789, numBus = 982)
        busesList.add(bus1)
        busesList.add(bus2)

        Mockito.`when`(busServiceMock.obtenerBuses())
            .thenReturn(busesList)

        given()
            .`when`().get("/buses/obtener-buses")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("size()", `is` (2))
            .body("idBus", hasItem(123456), "idBus", hasItem(456789))
    }

    // OBTENER REGISTRO POR SU ID

    @Test
    fun `debe retornar nulo cuando no existe el bus`() {
        Mockito.`when`(busServiceMock.obtenerBusPorID(123))
            .thenReturn(null)
        given()
            .`when`().get("/buses/obtener-bus-por-id/123")
            .then()
            .statusCode(204)
    }

    @Test
    fun `debe retornar un bus dado su id`() {
        val bus = Bus(123456, numBus = 980)

        Mockito.`when`(busServiceMock.obtenerBusPorID(123456))
            .thenReturn(bus)

        given()
            .`when`().get("/buses/obtener-bus-por-id/123456")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("idBus", `is`(123456))
    }

    // INSERTAR BUS

    @Test
    fun `debe retornar el bus ingresado`() {
        val bus = Bus(123456, numBus = 980)
        bus.idBus = 1

        Mockito.`when`(busServiceMock.insertarBus(bus))
            .thenReturn(bus)

        given()
            .contentType(ContentType.JSON)
            .body(bus)
            .`when`()
            .post("/buses/insertar-bus")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("idBus", `is`(1))
            .body("numBus", `is`(980))
    }

    // ACTUALIZAR BUS

    @Test
    fun `debe retornar el bus actualizado`() {

        val busActualizado = Bus(123456, numBus = 980)

        Mockito.`when`(busServiceMock.actualizarBus(busActualizado))
            .thenReturn(busActualizado)

        given()
            .contentType(ContentType.JSON)
            .body(busActualizado)
            .`when`()
            .put("/buses/actualizar-bus")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("idBus", `is`(123456))
            .body("numBus", `is`(980))
    }

    // ELIMINAR BUS

    @Test
    fun `debe retornar true si se elimino el bus`() {
        val busEliminar = Bus(123456, numBus = 980)

        Mockito.`when`(busServiceMock.eliminarBus(busEliminar))
            .thenReturn(true)

        given()
            .contentType(ContentType.JSON)
            .body(busEliminar)
            .`when`()
            .delete("/buses/eliminar-bus")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("eliminado", CoreMatchers.equalTo(true))
    }

}