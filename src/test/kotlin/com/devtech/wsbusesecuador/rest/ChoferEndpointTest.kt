package com.devtech.wsbusesecuador.rest

import com.devtech.wsbusesecuador.domain.Chofer
import com.devtech.wsbusesecuador.service.ChoferService
import io.quarkus.test.TestTransaction
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.mockito.InjectMock
import io.restassured.RestAssured.given
import io.restassured.http.ContentType
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.hasItem
import org.hamcrest.CoreMatchers.`is`
import org.junit.jupiter.api.Test
import org.mockito.Mockito

@QuarkusTest
@TestTransaction
class ChoferEndpointTest {

    @InjectMock
    lateinit var choferServiceMock: ChoferService

    // OBTENER TODOS LOS REGISTROS DE LA TABLA

    @Test
    fun `debe retornar nulo cuando no existan choferes`() {
        Mockito.`when`(choferServiceMock.obtenerChoferes())
            .thenReturn(null)
        given()
            .`when`().get("/choferes/obtener-choferes")
            .then()
            .statusCode(204)
    }

    @Test
    fun `debe retornar una lista de choferes`() {

        val choferesList = ArrayList<Chofer>()
        val chofer1 = Chofer(123456, "Manuel Naranjo", "1897645309")
        val chofer2 = Chofer(456789, "Manuel Perez", "1897645310")
        choferesList.add(chofer1)
        choferesList.add(chofer2)

        Mockito.`when`(choferServiceMock.obtenerChoferes())
            .thenReturn(choferesList)

        given()
            .`when`().get("/choferes/obtener-choferes")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("size()", `is` (2))
            .body("idChof", hasItem(123456), "idChof", hasItem(456789))
    }

    // OBTENER REGISTRO POR SU ID

    @Test
    fun `debe retornar nulo cuando no existe el chofer`() {
        Mockito.`when`(choferServiceMock.obtenerChoferPorID(123))
            .thenReturn(null)
        given()
            .`when`().get("/choferes/obtener-chofer-por-id/123")
            .then()
            .statusCode(204)
    }

    @Test
    fun `debe retornar un chofer dado su id`() {
        val chofer = Chofer(123456, "Manuel Naranjo", "1897645309")

        Mockito.`when`(choferServiceMock.obtenerChoferPorID(123456))
            .thenReturn(chofer)

        given()
            .`when`().get("/choferes/obtener-chofer-por-id/123456")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("idChof", `is`(123456))
    }

    // INSERTAR CHOFER

    @Test
    fun `debe retornar el chofer ingresado`() {
        val chofer = Chofer(123456, "Manuel Naranjo", "1897645309")
        chofer.idChof = 1

        Mockito.`when`(choferServiceMock.insertarChofer(chofer))
            .thenReturn(chofer)

        given()
            .contentType(ContentType.JSON)
            .body(chofer)
            .`when`()
            .post("/choferes/insertar-chofer")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("idChof", `is`(1))
            .body("nombreChof", `is`("Manuel Naranjo"))
            .body("cedChof", `is`("1897645309"))
    }

    // ACTUALIZAR CHOFER

    @Test
    fun `debe retornar el chofer actualizado`() {

        val choferActualizado = Chofer(123456, "Manuel Naranjo", "1897645309")

        Mockito.`when`(choferServiceMock.actualizarChofer(choferActualizado))
            .thenReturn(choferActualizado)

        given()
            .contentType(ContentType.JSON)
            .body(choferActualizado)
            .`when`()
            .put("/choferes/actualizar-chofer")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("idChof", `is`(123456))
            .body("nombreChof", `is`("Manuel Naranjo"))
            .body("cedChof", `is`("1897645309"))
    }

    // ELIMINAR CHOFER

    @Test
    fun `debe retornar true si se elimino el chofer`() {
        val choferEliminar = Chofer(123456, "Manuel Naranjo", "1897645309")

        Mockito.`when`(choferServiceMock.eliminarChofer(choferEliminar))
            .thenReturn(true)

        given()
            .contentType(ContentType.JSON)
            .body(choferEliminar)
            .`when`()
            .delete("/choferes/eliminar-chofer")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("eliminado", CoreMatchers.equalTo(true))
    }

}