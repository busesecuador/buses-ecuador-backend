package com.devtech.wsbusesecuador.rest

import com.devtech.wsbusesecuador.domain.Ruta
import com.devtech.wsbusesecuador.service.RutaService
import io.quarkus.test.TestTransaction
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.mockito.InjectMock
import io.restassured.RestAssured.given
import io.restassured.http.ContentType
import org.hamcrest.CoreMatchers.*
import org.junit.jupiter.api.Test
import org.mockito.Mockito

@QuarkusTest
@TestTransaction
class RutaEndpointTest {

    @InjectMock
    lateinit var rutaServiceMock: RutaService

    // OBTENER TODOS LOS REGISTROS DE LA TABLA

    @Test
    fun `debe retornar nulo cuando no existan rutas`() {
        Mockito.`when`(rutaServiceMock.obtenerRutas())
            .thenReturn(null)
        given()
            .`when`().get("/rutas/obtener-rutas")
            .then()
            .statusCode(204)
    }

    @Test
    fun `debe retornar una lista de rutas`() {

        val rutaList = ArrayList<Ruta>()
        val ruta1 = Ruta(123, precioRuta = 10.99f)
        val ruta2 = Ruta(456, precioRuta = 11.99f)
        rutaList.add(ruta1)
        rutaList.add(ruta2)

        Mockito.`when`(rutaServiceMock.obtenerRutas())
            .thenReturn(rutaList)

        given()
            .`when`().get("/rutas/obtener-rutas")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("size()", `is` (2))
            .body("idRuta", hasItem(123), "idRuta", hasItem(456))
    }

    // OBTENER REGISTRO POR SU ID

    @Test
    fun `debe retornar nulo cuando no existe la ruta`() {
        Mockito.`when`(rutaServiceMock.obtenerRutaPorID(123))
            .thenReturn(null)
        given()
            .`when`().get("/rutas/obtener-ruta-por-id/123")
            .then()
            .statusCode(204)
    }

    @Test
    fun `debe retornar una ruta dado su id`() {
        val ruta = Ruta(123, precioRuta = 10.99f)

        Mockito.`when`(rutaServiceMock.obtenerRutaPorID(123))
            .thenReturn(ruta)

        given()
            .`when`().get("/rutas/obtener-ruta-por-id/123")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("idRuta", `is`(123))
    }

    // INSERTAR RUTA

    @Test
    fun `debe retornar la ruta ingresada`() {
        val ruta = Ruta(123, precioRuta = 10.99f)
        ruta.idRuta = 1

        Mockito.`when`(rutaServiceMock.insertarRuta(ruta))
            .thenReturn(ruta)

        given()
            .contentType(ContentType.JSON)
            .body(ruta)
            .`when`()
            .post("/rutas/insertar-ruta")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("idRuta", `is`(1))
            .body("precioRuta", `is`(10.99f))
    }

    // ACTUALIZAR RUTA

    @Test
    fun `debe retornar la ruta actualizada`() {

        val rutaActualizada = Ruta(123, precioRuta = 10.99f)

        Mockito.`when`(rutaServiceMock.actualizarRuta(rutaActualizada))
            .thenReturn(rutaActualizada)

        given()
            .contentType(ContentType.JSON)
            .body(rutaActualizada)
            .`when`()
            .put("/rutas/actualizar-ruta")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("idRuta", `is`(123))
            .body("precioRuta", `is`(10.99f))
    }

    // ELIMINAR RUTA

    @Test
    fun `debe retornar true si se elimino la ruta`() {
        val rutaEliminar = Ruta(123, precioRuta = 10.99f)

        Mockito.`when`(rutaServiceMock.eliminarRuta(rutaEliminar))
            .thenReturn(true)

        given()
            .contentType(ContentType.JSON)
            .body(rutaEliminar)
            .`when`()
            .delete("/rutas/eliminar-ruta")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("eliminado", equalTo(true))
    }

    // SERVICIOS ADICIONALES

    @Test
    fun `debe retornar una lista de rutas por ciudad de inicio`() {
        val rutaList = ArrayList<Ruta>()
        rutaList.add(Ruta(123, precioRuta = 10.99f, ciudadIniRuta = "QUITO"))
        rutaList.add(Ruta(456, precioRuta = 10.99f, ciudadIniRuta = "QUITO"))

        Mockito.`when`(rutaServiceMock.obtenerRutasPorCiudadInicio("QUITO"))
            .thenReturn(rutaList)

        given()
            .`when`().get("/rutas/obtener-rutas-por-ciudad-inicio/QUITO")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("size()", `is`(2))
    }

}