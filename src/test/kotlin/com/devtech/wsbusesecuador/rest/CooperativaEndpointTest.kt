package com.devtech.wsbusesecuador.rest

import com.devtech.wsbusesecuador.domain.Cooperativa
import com.devtech.wsbusesecuador.dto.EliminadoDTO
import com.devtech.wsbusesecuador.service.CooperativaService
import io.quarkus.test.TestTransaction
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.mockito.InjectMock
import io.restassured.RestAssured.given
import io.restassured.http.ContentType
import org.hamcrest.CoreMatchers.*
import org.junit.jupiter.api.Test
import org.mockito.Mockito

@QuarkusTest
@TestTransaction
class CooperativaEndpointTest {

    @InjectMock
    lateinit var cooperativaServiceMock: CooperativaService

    // OBTENER TODOS LOS REGISTROS DE LA TABLA

    @Test
    fun `debe retornar nulo cuando no existan cooperativas`() {
        Mockito.`when`(cooperativaServiceMock.obtenerCooperativas())
            .thenReturn(null)
        given()
            .`when`().get("/cooperativas/obtener-cooperativas")
            .then()
            .statusCode(204)
    }

    @Test
    fun `debe retornar una lista de cooperativas`() {

        val cooperativaList = ArrayList<Cooperativa>()
        val cooperativa1 = Cooperativa(123, "Coop 1", 3)
        val cooperativa2 = Cooperativa(456, "Coop 2", 3)
        cooperativaList.add(cooperativa1)
        cooperativaList.add(cooperativa2)

        Mockito.`when`(cooperativaServiceMock.obtenerCooperativas())
            .thenReturn(cooperativaList)

        given()
            .`when`().get("/cooperativas/obtener-cooperativas")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("size()", `is` (2))
            .body("idCoop", hasItem(123), "idCoop", hasItem(456))
    }

    // OBTENER REGISTRO POR SU ID

    @Test
    fun `debe retornar nulo cuando no existe la cooperativas`() {
        Mockito.`when`(cooperativaServiceMock.obtenerCooperativaPorID(123))
            .thenReturn(null)
        given()
            .`when`().get("/cooperativas/obtener-cooperativa-por-id/123")
            .then()
            .statusCode(204)
    }

    @Test
    fun `debe retornar una cooperativa dado su id`() {
        val cooperativa = Cooperativa(123, "Coop 1", 3)

        Mockito.`when`(cooperativaServiceMock.obtenerCooperativaPorID(123))
            .thenReturn(cooperativa)

        given()
            .`when`().get("/cooperativas/obtener-cooperativa-por-id/123")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("idCoop", `is`(123))
    }

    // INSERTAR COOPERATIVA

    @Test
    fun `debe retornar la cooperativa ingresada`() {
        val cooperativa = Cooperativa(123, "Nueva Cooperativa", 3)
        cooperativa.idCoop = 1

        Mockito.`when`(cooperativaServiceMock.insertarCooperativa(cooperativa))
            .thenReturn(cooperativa)

        given()
            .contentType(ContentType.JSON)
            .body(cooperativa)
            .`when`()
            .post("/cooperativas/insertar-cooperativa")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("idCoop", `is`(1))
            .body("nombreCoop", `is`("Nueva Cooperativa"))
            .body("cantBusesCoop", `is`(3))
    }

    // ACTUALIZAR COOPERATIVA

    @Test
    fun `debe retornar la cooperativa actualizada`() {

        val cooperativaActualizada = Cooperativa(123, "Nueva Cooperativa", 3)

        Mockito.`when`(cooperativaServiceMock.actualizarCooperativa(cooperativaActualizada))
            .thenReturn(cooperativaActualizada)

        given()
            .contentType(ContentType.JSON)
            .body(cooperativaActualizada)
            .`when`()
            .put("/cooperativas/actualizar-cooperativa")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("idCoop", `is`(123))
            .body("nombreCoop", `is`("Nueva Cooperativa"))
            .body("cantBusesCoop", `is`(3))
    }

    // ELIMINAR COOPERATIVA

    @Test
    fun `debe retornar true si se elimino la cooperativa`() {
        val cooperativaEliminar = Cooperativa(123, "Cooperativa", 3)

        Mockito.`when`(cooperativaServiceMock.eliminarCooperativa(cooperativaEliminar))
            .thenReturn(true)

        given()
            .contentType(ContentType.JSON)
            .body(cooperativaEliminar)
            .`when`()
            .delete("/cooperativas/eliminar-cooperativa")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("eliminado", equalTo(true))
    }

}