package com.devtech.wsbusesecuador.rest

import com.devtech.wsbusesecuador.domain.Frecuencia
import com.devtech.wsbusesecuador.service.FrecuenciaService
import io.quarkus.test.TestTransaction
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.mockito.InjectMock
import io.restassured.RestAssured.given
import io.restassured.http.ContentType
import org.hamcrest.CoreMatchers.*
import org.junit.jupiter.api.Test
import org.mockito.Mockito

@QuarkusTest
@TestTransaction
class FrecuenciaEndpointTest {

    @InjectMock
    lateinit var frecuenciaServiceMock: FrecuenciaService

    // OBTENER TODOS LOS REGISTROS DE LA TABLA

    @Test
    fun `debe retornar nulo cuando no existan frecuencias`() {
        Mockito.`when`(frecuenciaServiceMock.obtenerFrecuencias())
            .thenReturn(null)
        given()
            .`when`().get("/frecuencias/obtener-frecuencias")
            .then()
            .statusCode(204)
    }

    @Test
    fun `debe retornar una lista de frecuencias`() {

        val frecuenciaList = ArrayList<Frecuencia>()
        val frecuencia1 = Frecuencia(123, habilitadaFrec = "Y")
        val frecuencia2 = Frecuencia(456, habilitadaFrec = "N")
        frecuenciaList.add(frecuencia1)
        frecuenciaList.add(frecuencia2)

        Mockito.`when`(frecuenciaServiceMock.obtenerFrecuencias())
            .thenReturn(frecuenciaList)

        given()
            .`when`().get("/frecuencias/obtener-frecuencias")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("size()", `is` (2))
            .body("idFrec", hasItem(123), "idFrec", hasItem(456))
    }

    // OBTENER REGISTRO POR SU ID

    @Test
    fun `debe retornar nulo cuando no existe la frecuencia`() {
        Mockito.`when`(frecuenciaServiceMock.obtenerFrecuenciaPorID(123))
            .thenReturn(null)
        given()
            .`when`().get("/frecuencias/obtener-frecuencia-por-id/123")
            .then()
            .statusCode(204)
    }

    @Test
    fun `debe retornar una frecuencia dado su id`() {
        val frecuencia = Frecuencia(123, habilitadaFrec = "Y")

        Mockito.`when`(frecuenciaServiceMock.obtenerFrecuenciaPorID(123))
            .thenReturn(frecuencia)

        given()
            .`when`().get("/frecuencias/obtener-frecuencia-por-id/123")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("idFrec", `is`(123))
    }

    // INSERTAR FRECUENCIA

    @Test
    fun `debe retornar la frecuencia ingresada`() {
        val frecuencia = Frecuencia(123, habilitadaFrec = "Y")
        frecuencia.idFrec = 1

        Mockito.`when`(frecuenciaServiceMock.insertarFrecuencia(frecuencia))
            .thenReturn(frecuencia)

        given()
            .contentType(ContentType.JSON)
            .body(frecuencia)
            .`when`()
            .post("/frecuencias/insertar-frecuencia")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("idFrec", `is`(1))
            .body("habilitadaFrec", `is`("Y"))
    }

    // ACTUALIZAR FRECUENCIA

    @Test
    fun `debe retornar la frecuencia actualizada`() {

        val frecuenciaActualizada = Frecuencia(123, habilitadaFrec = "Y")

        Mockito.`when`(frecuenciaServiceMock.actualizarFrecuencia(frecuenciaActualizada))
            .thenReturn(frecuenciaActualizada)

        given()
            .contentType(ContentType.JSON)
            .body(frecuenciaActualizada)
            .`when`()
            .put("/frecuencias/actualizar-frecuencia")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("idFrec", `is`(123))
            .body("habilitadaFrec", `is`("Y"))
    }

    // ELIMINAR FRECUENCIA

    @Test
    fun `debe retornar true si se elimino la frecuencia`() {
        val frecuenciaEliminar = Frecuencia(123, habilitadaFrec = "Y")

        Mockito.`when`(frecuenciaServiceMock.eliminarFrecuencia(frecuenciaEliminar))
            .thenReturn(true)

        given()
            .contentType(ContentType.JSON)
            .body(frecuenciaEliminar)
            .`when`()
            .delete("/frecuencias/eliminar-frecuencia")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("eliminado", equalTo(true))
    }

}