package com.devtech.wsbusesecuador.rest

import com.devtech.wsbusesecuador.domain.Persona
import com.devtech.wsbusesecuador.dto.LoginParamsDTO
import com.devtech.wsbusesecuador.service.PersonaService
import io.quarkus.test.TestTransaction
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.mockito.InjectMock
import io.restassured.RestAssured.given
import io.restassured.http.ContentType
import org.hamcrest.CoreMatchers.*
import org.junit.jupiter.api.Test
import org.mockito.Mockito

@QuarkusTest
@TestTransaction
class PersonaEndpointTest {

    @InjectMock
    lateinit var personaServiceMock: PersonaService

    // OBTENER TODOS LOS REGISTROS DE LA TABLA

    @Test
    fun `debe retornar nulo cuando no existan personas`() {
        Mockito.`when`(personaServiceMock.obtenerPersonas())
            .thenReturn(null)
        given()
            .`when`().get("/personas/obtener-personas")
            .then()
            .statusCode(204)
    }

    @Test
    fun `debe retornar una lista de personas`() {

        val personaList = ArrayList<Persona>()
        val persona1 = Persona(123456, "Manuel Naranjo", "1897645309")
        val persona2 = Persona(654321, "Manuel Jose Naranjo", "1897645310")
        personaList.add(persona1)
        personaList.add(persona2)

        Mockito.`when`(personaServiceMock.obtenerPersonas())
            .thenReturn(personaList)

        given()
            .`when`().get("/personas/obtener-personas")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("size()", `is` (2))
            .body("idPers", hasItem(123456), "idPers", hasItem(654321))
    }

    // OBTENER REGISTRO POR SU ID

    @Test
    fun `debe retornar nulo cuando no existe la personas`() {
        Mockito.`when`(personaServiceMock.obtenerPersonaPorID(123456))
            .thenReturn(null)
        given()
            .`when`().get("/personas/obtener-persona-por-id/123456")
            .then()
            .statusCode(204)
    }

    @Test
    fun `debe retornar una persona dado su id`() {
        val persona = Persona(123456, "Manuel Naranjo", "1897645309")

        Mockito.`when`(personaServiceMock.obtenerPersonaPorID(123456))
            .thenReturn(persona)

        given()
            .`when`().get("/personas/obtener-persona-por-id/123456")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("idPers", `is`(123456))
    }

    // INSERTAR PERSONA

    @Test
    fun `debe retornar la persona ingresada`() {
        val persona = Persona(123456, "Manuel Naranjo", "1897645309")
        persona.idPers = 1

        Mockito.`when`(personaServiceMock.insertarPersona(persona))
            .thenReturn(persona)

        given()
            .contentType(ContentType.JSON)
            .body(persona)
            .`when`()
            .post("/personas/insertar-persona")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("idPers", `is`(1))
            .body("nombrePers", `is`("Manuel Naranjo"))
            .body("cedPers", `is`("1897645309"))
    }

    // ACTUALIZAR PERSONA

    @Test
    fun `debe retornar la persona actualizada`() {

        val personaActualizada = Persona(123456, "Manuel Naranjo", "1897645309")

        Mockito.`when`(personaServiceMock.actualizarPersona(personaActualizada))
            .thenReturn(personaActualizada)

        given()
            .contentType(ContentType.JSON)
            .body(personaActualizada)
            .`when`()
            .put("/personas/actualizar-persona")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("idPers", `is`(123456))
            .body("nombrePers", `is`("Manuel Naranjo"))
            .body("cedPers", `is`("1897645309"))
    }

    // ELIMINAR PERSONA

    @Test
    fun `debe retornar true si se elimino la persona`() {
        val personaEliminar = Persona(123456, "Manuel Naranjo", "1897645309")

        Mockito.`when`(personaServiceMock.eliminarPersona(personaEliminar))
            .thenReturn(true)

        given()
            .contentType(ContentType.JSON)
            .body(personaEliminar)
            .`when`()
            .delete("/personas/eliminar-persona")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("eliminado", equalTo(true))
    }

    // LOGEAR PERSONA

    @Test
    fun `debe retornar nulo si no se pudo logear el usuario`() {
        val loginParamsDTO = LoginParamsDTO("mnaranjo", "pass12345")

        Mockito.`when`(personaServiceMock.login("mnaranjo", "pass12345"))
            .thenReturn(null)

        given()
            .contentType(ContentType.JSON)
            .body(loginParamsDTO)
            .`when`()
            .post("/personas/login")
            .then()
            .statusCode(204)
    }

    @Test
    fun `debe retornar el usuario logeado`() {
        val loginParamsDTO = LoginParamsDTO("mnaranjo", "pass12345")

        val persona = Persona(123456, "Manuel Naranjo", "1897645309", "mnaranjo", "pass12345", rolPers = "admin")

        Mockito.`when`(personaServiceMock.login("mnaranjo", "pass12345"))
            .thenReturn(persona)

        given()
            .contentType(ContentType.JSON)
            .body(loginParamsDTO)
            .`when`()
            .post("/personas/login")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("idPers", `is`(123456))
            .body("nombrePers", `is`("Manuel Naranjo"))
            .body("cedPers", `is`("1897645309"))
            .body("rolPers", `is`("admin"))
    }

}