package com.devtech.wsbusesecuador.rest

import com.devtech.wsbusesecuador.domain.Asiento
import com.devtech.wsbusesecuador.service.AsientoService
import io.quarkus.test.TestTransaction
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.mockito.InjectMock
import io.restassured.RestAssured.given
import io.restassured.http.ContentType
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.hasItem
import org.hamcrest.CoreMatchers.`is`
import org.junit.jupiter.api.Test
import org.mockito.Mockito

@QuarkusTest
@TestTransaction
class AsientoEndpointTest {

    @InjectMock
    lateinit var asientoServiceMock: AsientoService

    // OBTENER TODOS LOS REGISTROS DE LA TABLA

    @Test
    fun `debe retornar nulo cuando no existan asientos`() {
        Mockito.`when`(asientoServiceMock.obtenerAsientos())
            .thenReturn(null)
        given()
            .`when`().get("/asientos/obtener-asientos")
            .then()
            .statusCode(204)
    }

    @Test
    fun `debe retornar una lista de asientos`() {

        val asientoList = ArrayList<Asiento>()
        val asiento1 = Asiento(123456, tipoAsien = "NORMAL")
        val asiento2 = Asiento(456789, tipoAsien = "VIP")
        asientoList.add(asiento1)
        asientoList.add(asiento2)

        Mockito.`when`(asientoServiceMock.obtenerAsientos())
            .thenReturn(asientoList)

        given()
            .`when`().get("/asientos/obtener-asientos")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("size()", `is` (2))
            .body("idAsien", hasItem(123456), "idAsien", hasItem(456789))
    }

    // OBTENER REGISTRO POR SU ID

    @Test
    fun `debe retornar nulo cuando no existe el asiento`() {
        Mockito.`when`(asientoServiceMock.obtenerAsientoPorID(123))
            .thenReturn(null)
        given()
            .`when`().get("/asientos/obtener-asiento-por-id/123")
            .then()
            .statusCode(204)
    }

    @Test
    fun `debe retornar un asiento dado su id`() {
        val asiento = Asiento(123456, tipoAsien = "NORMAL")

        Mockito.`when`(asientoServiceMock.obtenerAsientoPorID(123456))
            .thenReturn(asiento)

        given()
            .`when`().get("/asientos/obtener-asiento-por-id/123456")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("idAsien", `is`(123456))
    }

    // INSERTAR ASIENTO

    @Test
    fun `debe retornar el asiento ingresado`() {
        val asiento = Asiento(123456, tipoAsien = "NORMAL")
        asiento.idAsien = 1

        Mockito.`when`(asientoServiceMock.insertarAsiento(asiento))
            .thenReturn(asiento)

        given()
            .contentType(ContentType.JSON)
            .body(asiento)
            .`when`()
            .post("/asientos/insertar-asiento")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("idAsien", `is`(1))
            .body("tipoAsien", `is`("NORMAL"))
    }

    // ACTUALIZAR ASIENTO

    @Test
    fun `debe retornar el asiento actualizado`() {

        val asientoActualizado = Asiento(123456, tipoAsien = "NORMAL")

        Mockito.`when`(asientoServiceMock.actualizarAsiento(asientoActualizado))
            .thenReturn(asientoActualizado)

        given()
            .contentType(ContentType.JSON)
            .body(asientoActualizado)
            .`when`()
            .put("/asientos/actualizar-asiento")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("idAsien", `is`(123456))
            .body("tipoAsien", `is`("NORMAL"))
    }

    // ELIMINAR ASIENTO

    @Test
    fun `debe retornar true si se elimino el asiento`() {
        val asientoEliminar = Asiento(123456, tipoAsien = "NORMAL")

        Mockito.`when`(asientoServiceMock.eliminarAsiento(asientoEliminar))
            .thenReturn(true)

        given()
            .contentType(ContentType.JSON)
            .body(asientoEliminar)
            .`when`()
            .delete("/asientos/eliminar-asiento")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("eliminado", CoreMatchers.equalTo(true))
    }

}