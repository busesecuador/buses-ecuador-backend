package com.devtech.wsbusesecuador.rest

import com.devtech.wsbusesecuador.domain.Boleto
import com.devtech.wsbusesecuador.service.BoletoService
import io.quarkus.test.TestTransaction
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.mockito.InjectMock
import io.restassured.RestAssured.given
import io.restassured.http.ContentType
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.hasItem
import org.hamcrest.CoreMatchers.`is`
import org.junit.jupiter.api.Test
import org.mockito.Mockito

@QuarkusTest
@TestTransaction
class BoletoEndpointTest {

    @InjectMock
    lateinit var boletoServiceMock: BoletoService

    // OBTENER TODOS LOS REGISTROS DE LA TABLA

    @Test
    fun `debe retornar nulo cuando no existan boletos`() {
        Mockito.`when`(boletoServiceMock.obtenerBoletos())
            .thenReturn(null)
        given()
            .`when`().get("/boletos/obtener-boletos")
            .then()
            .statusCode(204)
    }

    @Test
    fun `debe retornar una lista de boletos`() {

        val boletoList = ArrayList<Boleto>()
        val boleto1 = Boleto(123456, comprobPagoBol  = "987654")
        val boleto2 = Boleto(456789, comprobPagoBol  = "456789")
        boletoList.add(boleto1)
        boletoList.add(boleto2)

        Mockito.`when`(boletoServiceMock.obtenerBoletos())
            .thenReturn(boletoList)

        given()
            .`when`().get("/boletos/obtener-boletos")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("size()", `is` (2))
            .body("idBol", hasItem(123456), "idBol", hasItem(456789))
    }

    // OBTENER REGISTRO POR SU ID

    @Test
    fun `debe retornar nulo cuando no existe el boleto`() {
        Mockito.`when`(boletoServiceMock.obtenerBoletoPorID(123))
            .thenReturn(null)
        given()
            .`when`().get("/boletos/obtener-boleto-por-id/123")
            .then()
            .statusCode(204)
    }

    @Test
    fun `debe retornar un boleto dado su id`() {
        val boleto = Boleto(123456, comprobPagoBol  = "987654")

        Mockito.`when`(boletoServiceMock.obtenerBoletoPorID(123456))
            .thenReturn(boleto)

        given()
            .`when`().get("/boletos/obtener-boleto-por-id/123456")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("idBol", `is`(123456))
    }

    // INSERTAR BOLETO

    @Test
    fun `debe retornar el boleto ingresado`() {
        val boleto = Boleto(123456, comprobPagoBol  = "987654")
        boleto.idBol = 1

        Mockito.`when`(boletoServiceMock.insertarBoleto(boleto))
            .thenReturn(boleto)

        given()
            .contentType(ContentType.JSON)
            .body(boleto)
            .`when`()
            .post("/boletos/insertar-boleto")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("idBol", `is`(1))
            .body("comprobPagoBol", `is`("987654"))
    }

    // ACTUALIZAR BOLETO

    @Test
    fun `debe retornar el boleto actualizado`() {

        val boletoActualizado = Boleto(123456, comprobPagoBol  = "987654")

        Mockito.`when`(boletoServiceMock.actualizarBoleto(boletoActualizado))
            .thenReturn(boletoActualizado)

        given()
            .contentType(ContentType.JSON)
            .body(boletoActualizado)
            .`when`()
            .put("/boletos/actualizar-boleto")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("idBol", `is`(123456))
            .body("comprobPagoBol", `is`("987654"))
    }

    // ELIMINAR BOLETO

    @Test
    fun `debe retornar true si se elimino el boleto`() {
        val boletoEliminar = Boleto(123456, comprobPagoBol  = "987654")

        Mockito.`when`(boletoServiceMock.eliminarBoleto(boletoEliminar))
            .thenReturn(true)

        given()
            .contentType(ContentType.JSON)
            .body(boletoEliminar)
            .`when`()
            .delete("/boletos/eliminar-boleto")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("eliminado", CoreMatchers.equalTo(true))
    }

}