package com.devtech.wsbusesecuador.rest

import com.devtech.wsbusesecuador.domain.Asiento
import com.devtech.wsbusesecuador.dto.EliminadoDTO
import com.devtech.wsbusesecuador.service.AsientoService
import org.slf4j.LoggerFactory
import javax.inject.Inject
import javax.ws.rs.*
import javax.ws.rs.core.*

@Path("asientos")
class AsientoEndpoint {

    @Inject
    private lateinit var asientoService: AsientoService

    private val log = LoggerFactory.getLogger(javaClass)

    @GET
    @Path("/obtener-asientos")
    fun obtenerAsientos(): List<Asiento>? {
        log.debug("Petición REST para obtener todos los asientos")

        if (asientoService.obtenerAsientos().isNullOrEmpty())
            return null

        return asientoService.obtenerAsientos()
    }

    @GET
    @Path("/obtener-asiento-por-id/{idAsien}")
    fun obtenerAsientoPorId(@PathParam("idAsien") idAsien: Long): Asiento? {
        log.debug("Petición REST para obtener un asiento por su id: $idAsien")

        return asientoService.obtenerAsientoPorID(idAsien)
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/insertar-asiento")
    fun insertarAsiento(asiento: Asiento): Asiento? {
        log.debug("Petición REST para insertar un asiento")

        return asientoService.insertarAsiento(asiento)
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/actualizar-asiento")
    fun actualizarAsiento(asiento: Asiento): Asiento? {
        log.debug("Petición REST para actualizar un asiento")

        return asientoService.actualizarAsiento(asiento)
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/eliminar-asiento")
    fun eliminarAsiento(asiento: Asiento): EliminadoDTO {
        log.debug("Petición REST para eliminar un asiento")

        val eliminadoDTO = EliminadoDTO(true)

        if (!asientoService.eliminarAsiento(asiento))
            eliminadoDTO.eliminado = false

        return eliminadoDTO
    }

}