package com.devtech.wsbusesecuador.rest

import com.devtech.wsbusesecuador.domain.Ruta
import com.devtech.wsbusesecuador.dto.DetalleDeRutaDTO
import com.devtech.wsbusesecuador.dto.EliminadoDTO
import com.devtech.wsbusesecuador.service.RutaService
import org.slf4j.LoggerFactory
import javax.inject.Inject
import javax.ws.rs.*
import javax.ws.rs.core.*

@Path("rutas")
class RutaEndpoint {

    @Inject
    private lateinit var rutaService: RutaService

    private val log = LoggerFactory.getLogger(javaClass)

    @GET
    @Path("/obtener-rutas")
    fun obtenerRutas(): List<Ruta>? {
        log.debug("Petición REST para obtener todas las rutas")

        if (rutaService.obtenerRutas().isNullOrEmpty())
            return null

        return rutaService.obtenerRutas()
    }

    @GET
    @Path("/obtener-ruta-por-id/{idRuta}")
    fun obtenerRutaPorID(@PathParam("idRuta") idRuta: Long): Ruta? {
        log.debug("Petición REST para obtener una ruta por su id: $idRuta")

        return rutaService.obtenerRutaPorID(idRuta)
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/insertar-ruta")
    fun insertarRuta(ruta: Ruta): Ruta? {
        log.debug("Petición REST para ruta una ruta")

        return rutaService.insertarRuta(ruta)
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/actualizar-ruta")
    fun actualizarRuta(ruta: Ruta): Ruta? {
        log.debug("Petición REST para actualizar una ruta")

        return rutaService.actualizarRuta(ruta)
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/eliminar-ruta")
    fun eliminarRuta(ruta: Ruta): EliminadoDTO {
        log.debug("Petición REST para eliminar una ruta")

        val eliminadoDTO = EliminadoDTO(true)

        if (!rutaService.eliminarRuta(ruta))
            eliminadoDTO.eliminado = false

        return eliminadoDTO
    }

    @GET
    @Path("/obtener-rutas-por-ciudad-inicio/{ciudadInicio}")
    fun obtenerRutaPorID(@PathParam("ciudadInicio") ciudadInicio: String): List<Ruta>? {
        log.debug("Petición REST para obtener una lista de rutas por su ciudad de inicio: $ciudadInicio")

        val rutaList = rutaService.obtenerRutasPorCiudadInicio(ciudadInicio)

        if (rutaList.isEmpty())
            return null

        return rutaList
    }

    @GET
    @Path("/obtener-detalle-de-ruta-completo-por-id/{idRuta}")
    fun obtenerDetalleDeRutaPorID(@PathParam("idRuta") idRuta: Long): DetalleDeRutaDTO? {
        log.debug("Petición REST para obtener todo el detalle de una ruta por su id: $idRuta")

        return rutaService.obtenerDetalleDeRutaPorID(idRuta)
    }

}