package com.devtech.wsbusesecuador.rest

import com.devtech.wsbusesecuador.domain.Chofer
import com.devtech.wsbusesecuador.dto.EliminadoDTO
import com.devtech.wsbusesecuador.service.ChoferService
import org.slf4j.LoggerFactory
import javax.inject.Inject
import javax.ws.rs.*
import javax.ws.rs.core.*

@Path("choferes")
class ChoferEndpoint {

    @Inject
    private lateinit var choferService: ChoferService

    private val log = LoggerFactory.getLogger(javaClass)

    @GET
    @Path("/obtener-choferes")
    fun obtenerChoferes(): List<Chofer>? {
        log.debug("Petición REST para obtener todos los choferes")

        if (choferService.obtenerChoferes().isNullOrEmpty())
            return null

        return choferService.obtenerChoferes()
    }

    @GET
    @Path("/obtener-chofer-por-id/{idChof}")
    fun obtenerChoferPorId(@PathParam("idChof") idChof: Long): Chofer? {
        log.debug("Petición REST para obtener un chofer por su id: $idChof")

        return choferService.obtenerChoferPorID(idChof)
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/insertar-chofer")
    fun insertarChofer(chofer: Chofer): Chofer? {
        log.debug("Petición REST para insertar un chofer")

        return choferService.insertarChofer(chofer)
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/actualizar-chofer")
    fun actualizarChofer(chofer: Chofer): Chofer? {
        log.debug("Petición REST para actualizar un chofer")

        return choferService.actualizarChofer(chofer)
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/eliminar-chofer")
    fun eliminarChofer(chofer: Chofer): EliminadoDTO {
        log.debug("Petición REST para eliminar un chofer")

        val eliminadoDTO = EliminadoDTO(true)

        if (!choferService.eliminarChofer(chofer))
            eliminadoDTO.eliminado = false

        return eliminadoDTO
    }

}