package com.devtech.wsbusesecuador.rest

import com.devtech.wsbusesecuador.domain.Boleto
import com.devtech.wsbusesecuador.dto.EliminadoDTO
import com.devtech.wsbusesecuador.service.BoletoService
import org.slf4j.LoggerFactory
import javax.inject.Inject
import javax.ws.rs.*
import javax.ws.rs.core.*

@Path("boletos")
class BoletoEndpoint {

    @Inject
    private lateinit var boletoService: BoletoService

    private val log = LoggerFactory.getLogger(javaClass)

    @GET
    @Path("/obtener-boletos")
    fun obtenerBoletos(): List<Boleto>? {
        log.debug("Petición REST para obtener todos los boletos")

        if (boletoService.obtenerBoletos().isNullOrEmpty())
            return null

        return boletoService.obtenerBoletos()
    }

    @GET
    @Path("/obtener-boleto-por-id/{idBol}")
    fun obtenerBoletoPorId(@PathParam("idBol") idBol: Long): Boleto? {
        log.debug("Petición REST para obtener un boleto por su id: $idBol")

        return boletoService.obtenerBoletoPorID(idBol)
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/insertar-boleto")
    fun insertarBoleto(boleto: Boleto): Boleto? {
        log.debug("Petición REST para insertar un boleto")

        return boletoService.insertarBoleto(boleto)
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/actualizar-boleto")
    fun actualizarBoleto(boleto: Boleto): Boleto? {
        log.debug("Petición REST para actualizar un Boleto")

        return boletoService.actualizarBoleto(boleto)
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/eliminar-boleto")
    fun eliminarBoleto(boleto: Boleto): EliminadoDTO {
        log.debug("Petición REST para eliminar un Boleto")

        val eliminadoDTO = EliminadoDTO(true)

        if (!boletoService.eliminarBoleto(boleto))
            eliminadoDTO.eliminado = false

        return eliminadoDTO
    }

}