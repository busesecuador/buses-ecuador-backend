package com.devtech.wsbusesecuador.rest

import com.devtech.wsbusesecuador.domain.Cooperativa
import com.devtech.wsbusesecuador.dto.EliminadoDTO
import com.devtech.wsbusesecuador.service.CooperativaService
import org.slf4j.LoggerFactory
import javax.inject.Inject
import javax.ws.rs.Consumes
import javax.ws.rs.DELETE
import javax.ws.rs.GET
import javax.ws.rs.POST
import javax.ws.rs.PUT
import javax.ws.rs.core.*
import javax.ws.rs.Path
import javax.ws.rs.PathParam
import javax.ws.rs.Produces

@Path("cooperativas")
class CooperativaEndpoint {

    @Inject
    private lateinit var cooperativaService: CooperativaService

    private val log = LoggerFactory.getLogger(javaClass)

    @GET
    @Path("/obtener-cooperativas")
    fun obtenerCooperativas(): List<Cooperativa>? {
        log.debug("Petición REST para obtener todas las cooperativas")

        if (cooperativaService.obtenerCooperativas().isNullOrEmpty())
            return null

        return cooperativaService.obtenerCooperativas()
    }

    @GET
    @Path("/obtener-cooperativa-por-id/{idCoop}")
    fun obtenerCooperativaPorId(@PathParam("idCoop") idCoop: Long): Cooperativa? {
        log.debug("Petición REST para obtener una cooperativa por su id: $idCoop")

        return cooperativaService.obtenerCooperativaPorID(idCoop)
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/insertar-cooperativa")
    fun insertarCooperativa(cooperativa: Cooperativa): Cooperativa? {
        log.debug("Petición REST para insertar una cooperativa")

        return cooperativaService.insertarCooperativa(cooperativa)
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/actualizar-cooperativa")
    fun actualizarCooperativa(cooperativa: Cooperativa): Cooperativa? {
        log.debug("Petición REST para actualizar una cooperativa")

        return cooperativaService.actualizarCooperativa(cooperativa)
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/eliminar-cooperativa")
    fun eliminarCooperativa(cooperativa: Cooperativa): EliminadoDTO {
        log.debug("Petición REST para eliminar una cooperativa")

        val eliminadoDTO = EliminadoDTO(true)

        if (!cooperativaService.eliminarCooperativa(cooperativa))
            eliminadoDTO.eliminado = false

        return eliminadoDTO
    }

}