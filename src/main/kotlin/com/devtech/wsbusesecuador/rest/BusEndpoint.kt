package com.devtech.wsbusesecuador.rest

import com.devtech.wsbusesecuador.domain.Bus
import com.devtech.wsbusesecuador.dto.EliminadoDTO
import com.devtech.wsbusesecuador.service.BusService
import org.slf4j.LoggerFactory
import javax.inject.Inject
import javax.ws.rs.*
import javax.ws.rs.core.*

@Path("buses")
class BusEndpoint {

    @Inject
    private lateinit var busService: BusService

    private val log = LoggerFactory.getLogger(javaClass)

    @GET
    @Path("/obtener-buses")
    fun obtenerBuses(): List<Bus>? {
        log.debug("Petición REST para obtener todos los buses")

        if (busService.obtenerBuses().isNullOrEmpty())
            return null

        return busService.obtenerBuses()
    }

    @GET
    @Path("/obtener-bus-por-id/{idBus}")
    fun obtenerBusPorId(@PathParam("idBus") idBus: Long): Bus? {
        log.debug("Petición REST para obtener un bus por su id: $idBus")

        return busService.obtenerBusPorID(idBus)
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/insertar-bus")
    fun insertarBus(bus: Bus): Bus? {
        log.debug("Petición REST para insertar un bus")

        return busService.insertarBus(bus)
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/actualizar-bus")
    fun actualizarBus(bus: Bus): Bus? {
        log.debug("Petición REST para actualizar un bus")

        return busService.actualizarBus(bus)
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/eliminar-bus")
    fun eliminarBus(bus: Bus): EliminadoDTO {
        log.debug("Petición REST para eliminar un bus")

        val eliminadoDTO = EliminadoDTO(true)

        if (!busService.eliminarBus(bus))
            eliminadoDTO.eliminado = false

        return eliminadoDTO
    }

}