package com.devtech.wsbusesecuador.rest

import com.devtech.wsbusesecuador.domain.Frecuencia
import com.devtech.wsbusesecuador.dto.EliminadoDTO
import com.devtech.wsbusesecuador.service.FrecuenciaService
import org.slf4j.LoggerFactory
import javax.inject.Inject
import javax.ws.rs.*
import javax.ws.rs.core.*

@Path("frecuencias")
class FrecuenciaEndpoint {

    @Inject
    private lateinit var frecuenciaService: FrecuenciaService

    private val log = LoggerFactory.getLogger(javaClass)

    @GET
    @Path("/obtener-frecuencias")
    fun obtenerFrecuencias(): List<Frecuencia>? {
        log.debug("Petición REST para obtener todas las frecuencias")

        if (frecuenciaService.obtenerFrecuencias().isNullOrEmpty())
            return null

        return frecuenciaService.obtenerFrecuencias()
    }

    @GET
    @Path("/obtener-frecuencia-por-id/{idFrec}")
    fun obtenerFrecuenciaPorId(@PathParam("idFrec") idFrec: Long): Frecuencia? {
        log.debug("Petición REST para obtener una frecuencia por su id: $idFrec")

        return frecuenciaService.obtenerFrecuenciaPorID(idFrec)
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/insertar-frecuencia")
    fun insertarFrecuencia(frecuencia: Frecuencia): Frecuencia? {
        log.debug("Petición REST para insertar una frecuencia")

        return frecuenciaService.insertarFrecuencia(frecuencia)
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/actualizar-frecuencia")
    fun actualizarFrecuencia(frecuencia: Frecuencia): Frecuencia? {
        log.debug("Petición REST para actualizar una frecuencia")

        return frecuenciaService.actualizarFrecuencia(frecuencia)
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/eliminar-frecuencia")
    fun eliminarFrecuencia(frecuencia: Frecuencia): EliminadoDTO {
        log.debug("Petición REST para eliminar una frecuencia")

        val eliminadoDTO = EliminadoDTO(true)

        if (!frecuenciaService.eliminarFrecuencia(frecuencia))
            eliminadoDTO.eliminado = false

        return eliminadoDTO
    }

}