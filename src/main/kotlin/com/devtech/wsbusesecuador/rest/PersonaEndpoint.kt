package com.devtech.wsbusesecuador.rest

import com.devtech.wsbusesecuador.domain.Persona
import com.devtech.wsbusesecuador.dto.EliminadoDTO
import com.devtech.wsbusesecuador.dto.LoginParamsDTO
import com.devtech.wsbusesecuador.service.PersonaService
import org.slf4j.LoggerFactory
import javax.inject.Inject
import javax.ws.rs.*
import javax.ws.rs.core.*

@Path("personas")
class PersonaEndpoint {

    @Inject
    private lateinit var personaService: PersonaService

    private val log = LoggerFactory.getLogger(javaClass)

    @GET
    @Path("/obtener-personas")
    fun obtenerPersonas(): List<Persona>? {
        log.debug("Petición REST para obtener todas las personas")

        if (personaService.obtenerPersonas().isNullOrEmpty())
            return null

        return personaService.obtenerPersonas()
    }

    @GET
    @Path("/obtener-persona-por-id/{idPers}")
    fun obtenerPersonaPorId(@PathParam("idPers") idPers: Long): Persona? {
        log.debug("Petición REST para obtener una persona por su id: $idPers")

        return personaService.obtenerPersonaPorID(idPers)
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/insertar-persona")
    fun insertarPersona(persona: Persona): Persona? {
        log.debug("Petición REST para insertar una persona")

        return personaService.insertarPersona(persona)
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/actualizar-persona")
    fun actualizarPersona(persona: Persona): Persona? {
        log.debug("Petición REST para actualizar una persona")

        return personaService.actualizarPersona(persona)
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/eliminar-persona")
    fun eliminarPersona(persona: Persona): EliminadoDTO {
        log.debug("Petición REST para eliminar una persona")

        val eliminadoDTO = EliminadoDTO(true)

        if (!personaService.eliminarPersona(persona))
            eliminadoDTO.eliminado = false

        return eliminadoDTO
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/login")
    fun login(loginParamsDTO: LoginParamsDTO): Persona? {
        log.debug("Petición REST para logear un usuario")

        return personaService.login(loginParamsDTO.username, loginParamsDTO.password)
    }

}