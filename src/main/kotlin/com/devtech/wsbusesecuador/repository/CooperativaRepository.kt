package com.devtech.wsbusesecuador.repository

import com.devtech.wsbusesecuador.domain.Cooperativa
import io.quarkus.hibernate.orm.panache.kotlin.PanacheRepository
import javax.enterprise.context.ApplicationScoped

/**
 * Repositorio que proporciona operaciones de acceso a datos para la entidad Cooperativa.
 *
 * @constructor Crea un nuevo repositorio de Cooperativa.
 *
 * @author MateoMartinez
 */
@ApplicationScoped
class CooperativaRepository : PanacheRepository<Cooperativa> {

    /**
     * Obtiene el máximo ID de toda la tabla de cooperativas.
     *
     * @return El máximo ID de la tabla de cooperativas o null si la tabla está vacía.
     */
    fun findMaxID(): Long?{
        val result = find("order by ID_COOP desc").list()

        if (result.isEmpty())
            return null

        return result[0].idCoop
    }
}
