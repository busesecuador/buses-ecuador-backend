package com.devtech.wsbusesecuador.repository

import com.devtech.wsbusesecuador.domain.Ruta
import io.quarkus.hibernate.orm.panache.kotlin.PanacheRepository
import javax.enterprise.context.ApplicationScoped

/**
 * Repositorio que proporciona operaciones de acceso a datos para la entidad Ruta.
 *
 * @constructor Crea un nuevo repositorio de Ruta.
 *
 * @author MateoMartinez
 */
@ApplicationScoped
class RutaRepository : PanacheRepository<Ruta> {

    /**
     * Obtiene el máximo ID de toda la tabla de rutas.
     *
     * @return El máximo ID de la tabla de rutas o null si la tabla está vacía.
     */
    fun findMaxID(): Long?{
        val result = find("order by ID_RUTA desc").list()

        if (result.isEmpty())
            return null

        return result[0].idRuta
    }

    /**
     * Obtiene una lista de rutas por su ciudad de inicio.
     *
     * @param ciudadInicio La ciudad de inicio de las rutas a buscar.
     * @return Una lista de rutas que coinciden con la ciudad de inicio especificada.
     */
    fun findByCiudadInicio(ciudadInicio: String): List<Ruta> {
        return find("ciudad_ini_ruta", ciudadInicio).list()
    }
}
