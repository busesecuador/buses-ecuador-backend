package com.devtech.wsbusesecuador.repository

import com.devtech.wsbusesecuador.domain.Boleto
import io.quarkus.hibernate.orm.panache.kotlin.PanacheRepository
import javax.enterprise.context.ApplicationScoped

/**
 * Repositorio que proporciona operaciones de acceso a datos para la entidad Boleto.
 *
 * @constructor Crea un nuevo repositorio de Boleto.
 *
 * @author MateoMartinez
 */
@ApplicationScoped
class BoletoRepository : PanacheRepository<Boleto> {

    /**
     * Obtiene el máximo ID de toda la tabla de boletos.
     *
     * @return El máximo ID de la tabla de boletos o null si la tabla está vacía.
     */
    fun findMaxID(): Long?{
        val result = find("order by ID_BOL desc").list()

        if (result.isEmpty())
            return null

        return result[0].idBol
    }
}
