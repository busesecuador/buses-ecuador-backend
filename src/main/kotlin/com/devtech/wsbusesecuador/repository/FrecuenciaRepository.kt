package com.devtech.wsbusesecuador.repository

import com.devtech.wsbusesecuador.domain.Frecuencia
import io.quarkus.hibernate.orm.panache.kotlin.PanacheRepository
import javax.enterprise.context.ApplicationScoped

/**
 * Repositorio que proporciona operaciones de acceso a datos para la entidad Frecuencia.
 *
 * @constructor Crea un nuevo repositorio de Frecuencia.
 *
 * @author MateoMartinez
 */
@ApplicationScoped
class FrecuenciaRepository : PanacheRepository<Frecuencia> {

    /**
     * Obtiene el máximo ID de toda la tabla de frecuencias.
     *
     * @return El máximo ID de la tabla de frecuencias o null si la tabla está vacía.
     */
    fun findMaxID(): Long?{
        val result = find("order by ID_FREC desc").list()

        if (result.isEmpty())
            return null

        return result[0].idFrec
    }
}
