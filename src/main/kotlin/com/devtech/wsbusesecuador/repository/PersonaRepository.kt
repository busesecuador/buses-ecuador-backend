package com.devtech.wsbusesecuador.repository

import com.devtech.wsbusesecuador.domain.Persona
import io.quarkus.hibernate.orm.panache.kotlin.PanacheRepository
import javax.enterprise.context.ApplicationScoped

/**
 * Repositorio que proporciona operaciones de acceso a datos para la entidad Persona.
 *
 * @constructor Crea un nuevo repositorio de Persona.
 *
 * @author MateoMartinez
 */
@ApplicationScoped
class PersonaRepository : PanacheRepository<Persona> {

    /**
     * Obtiene el máximo ID de toda la tabla de personas.
     *
     * @return El máximo ID de la tabla de personas o null si la tabla está vacía.
     */
    fun findMaxID(): Long?{
        val result = find("order by ID_PERS desc").list()

        if (result.isEmpty())
            return null

        return result[0].idPers
    }
}
