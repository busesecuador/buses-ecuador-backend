package com.devtech.wsbusesecuador.repository

import com.devtech.wsbusesecuador.domain.Bus
import io.quarkus.hibernate.orm.panache.kotlin.PanacheRepository
import javax.enterprise.context.ApplicationScoped

/**
 * Repositorio que proporciona operaciones de acceso a datos para la entidad Bus.
 *
 * @constructor Crea un nuevo repositorio de Bus.
 *
 * @author MateoMartinez
 */
@ApplicationScoped
class BusRepository : PanacheRepository<Bus> {

    /**
     * Obtiene el máximo ID de toda la tabla de buses.
     *
     * @return El máximo ID de la tabla de buses o null si la tabla está vacía.
     */
    fun findMaxID(): Long?{
        val result = find("order by ID_BUS desc").list()

        if (result.isEmpty())
            return null

        return result[0].idBus
    }
}
