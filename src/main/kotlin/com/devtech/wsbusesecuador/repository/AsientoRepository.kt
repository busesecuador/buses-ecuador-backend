package com.devtech.wsbusesecuador.repository

import com.devtech.wsbusesecuador.domain.Asiento
import io.quarkus.hibernate.orm.panache.kotlin.PanacheRepository
import javax.enterprise.context.ApplicationScoped

/**
 * Repositorio que proporciona operaciones de acceso a datos para la entidad Asiento.
 *
 * @constructor Crea un nuevo repositorio de Asiento.
 *
 * @author MateoMartinez
 */
@ApplicationScoped
class AsientoRepository : PanacheRepository<Asiento> {

    /**
     * Obtiene el máximo ID de toda la tabla de asientos.
     *
     * @return El máximo ID de la tabla de asientos o null si la tabla está vacía.
     */
    fun findMaxID(): Long?{
        val result = find("order by ID_ASIEN desc").list()

        if (result.isEmpty())
            return null

        return result[0].idAsien
    }
}