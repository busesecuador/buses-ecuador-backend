package com.devtech.wsbusesecuador.service


import com.devtech.wsbusesecuador.domain.Ruta
import com.devtech.wsbusesecuador.dto.DetalleDeRutaDTO
import com.devtech.wsbusesecuador.repository.RutaRepository
import org.slf4j.LoggerFactory
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject
import javax.transaction.Transactional

/**
 * Servicio que proporciona operaciones relacionadas con las rutas.
 *
 * @author MateoMartinez
 */
@ApplicationScoped
@Transactional
class RutaService {

    @Inject
    lateinit var rutaRepository: RutaRepository

    @Inject
    lateinit var frecuenciaService: FrecuenciaService

    @Inject
    lateinit var busService: BusService

    @Inject
    lateinit var cooperativaService: CooperativaService

    private val log = LoggerFactory.getLogger(javaClass)

    /**
     * Obtiene todas las rutas.
     *
     * @return Lista de objetos [Ruta] o null si no hay rutas disponibles.
     */
    fun obtenerRutas(): List<Ruta>? {
        log.debug("Servicio para obtener todas las rutas")

        return rutaRepository.findAll().list()
    }

    /**
     * Obtiene una ruta por su ID.
     *
     * @param idRuta ID de la ruta a buscar.
     * @return Objeto [Ruta] correspondiente al ID especificado o null si no se encuentra.
     */
    fun obtenerRutaPorID(idRuta: Long): Ruta? {
        log.debug("Servicio para obtener una ruta por su ID")

        return rutaRepository.findById(idRuta)
    }

    /**
     * Obtiene el máximo ID de toda la tabla de rutas.
     *
     * @return El máximo ID de la tabla de rutas o null si la tabla está vacía.
     */
    fun obtenerMaximoID(): Long? {
        log.debug("Servicio para obtener el máximo ID de toda la tabla")

        return rutaRepository.findMaxID()
    }

    /**
     * Inserta una nueva ruta.
     *
     * @param ruta Objeto [Ruta] a insertar.
     * @return Objeto [Ruta] insertado o null si no se pudo insertar.
     */
    fun insertarRuta(ruta: Ruta?): Ruta? {
        log.debug("Servicio para insertar una ruta")

        if (ruta == null)
            return null

        val maxID = obtenerMaximoID()
        ruta.idRuta = (maxID?.toInt()?.plus(1))?.toLong()!!

        rutaRepository.persist(ruta)

        if (obtenerRutaPorID(ruta.idRuta) == null)
            return null

        return ruta
    }

    /**
     * Actualiza los datos de una ruta.
     *
     * @param rutaNuevosDatos Objeto [Ruta] con los nuevos datos a actualizar.
     * @return Objeto [Ruta] actualizado o null si no se pudo actualizar.
     */
    fun actualizarRuta(rutaNuevosDatos: Ruta): Ruta? {
        log.debug("Servicio para actualizar una ruta")

        val rutaBD = obtenerRutaPorID(rutaNuevosDatos.idRuta) ?: return null

        rutaBD.idFrecRuta = rutaNuevosDatos.idFrecRuta
        rutaBD.fechaHoraSaliRuta = rutaNuevosDatos.fechaHoraSaliRuta
        rutaBD.fechaHoraLlegRuta = rutaNuevosDatos.fechaHoraLlegRuta
        rutaBD.ciudadIniRuta = rutaNuevosDatos.ciudadIniRuta
        rutaBD.ciudadFinRuta = rutaNuevosDatos.ciudadFinRuta
        rutaBD.precioRuta = rutaNuevosDatos.precioRuta

        return rutaBD
    }

    /**
     * Elimina una ruta.
     *
     * @param rutaEliminar Objeto [Ruta] a eliminar.
     * @return `true` si se eliminó correctamente, `false` si no se encontró la ruta.
     */
    fun eliminarRuta(rutaEliminar: Ruta): Boolean {
        log.debug("Servicio para eliminar una ruta")

        val rutaBD = obtenerRutaPorID(rutaEliminar.idRuta) ?: return false

        rutaRepository.delete(rutaBD)

        return true
    }

    /**
     * Obtiene las rutas por ciudad de inicio.
     *
     * @param ciudadInicio Ciudad de inicio a buscar.
     * @return Lista de objetos [Ruta] que tienen la ciudad de inicio especificada.
     */
    fun obtenerRutasPorCiudadInicio(ciudadInicio: String): List<Ruta> {
        log.debug("Servicio para obtener rutas por ciudad de inicio")

        return rutaRepository.findByCiudadInicio(ciudadInicio.trim())
    }

    /**
     * Obtiene el detalle de una ruta por su ID.
     *
     * @param idRuta ID de la ruta a obtener el detalle.
     * @return Objeto [DetalleDeRutaDTO] que contiene la información de la ruta, el bus y la cooperativa.
     *         null si no se encuentra la ruta, el bus o la cooperativa correspondiente.
     */
    fun obtenerDetalleDeRutaPorID(idRuta: Long): DetalleDeRutaDTO? {
        log.debug("Servicio para obtener ruta, bus, cooperativa por su ID")

        val ruta = obtenerRutaPorID(idRuta) ?: return null

        val frecuencia = frecuenciaService.obtenerFrecuenciaPorID(ruta.idFrecRuta) ?: return null

        val bus = busService.obtenerBusPorID(frecuencia.idBusFrec) ?: return null

        val cooperativa = cooperativaService.obtenerCooperativaPorID(bus.idCoopBus) ?: return null

        return DetalleDeRutaDTO(
            ruta.idRuta,
            ruta.idFrecRuta,
            ruta.fechaHoraSaliRuta,
            ruta.fechaHoraLlegRuta,
            ruta.ciudadIniRuta,
            ruta.ciudadFinRuta,
            ruta.precioRuta,
            bus.idBus,
            bus.idCoopBus,
            bus.idChofBus,
            bus.numBus,
            bus.cantAsienBus,
            bus.marcaChasisBus,
            bus.carroceriaBus,
            bus.placaBus,
            cooperativa.idCoop,
            cooperativa.nombreCoop,
            cooperativa.cantBusesCoop
        )

    }

}