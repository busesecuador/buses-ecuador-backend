package com.devtech.wsbusesecuador.service


import com.devtech.wsbusesecuador.domain.Cooperativa
import com.devtech.wsbusesecuador.repository.CooperativaRepository
import org.slf4j.LoggerFactory
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject
import javax.transaction.Transactional

/**
 * Servicio que proporciona operaciones relacionadas con las cooperativas.
 *
 * @author MateoMartinez
 */
@ApplicationScoped
@Transactional
class CooperativaService {

    @Inject
    lateinit var cooperativaRepository: CooperativaRepository

    private val log = LoggerFactory.getLogger(javaClass)

    /**
     * Obtiene todas las cooperativas.
     *
     * @return Lista de objetos [Cooperativa] o null si no hay cooperativas disponibles.
     */
    fun obtenerCooperativas(): List<Cooperativa>? {
        log.debug("Servicio para obtener todas las cooperativas")

        return cooperativaRepository.findAll().list()
    }

    /**
     * Obtiene una cooperativa por su ID.
     *
     * @param idCoop ID de la cooperativa a buscar.
     * @return Objeto [Cooperativa] correspondiente al ID especificado o null si no se encuentra.
     */
    fun obtenerCooperativaPorID(idCoop: Long): Cooperativa? {
        log.debug("Servicio para obtener una cooperativa por su ID")

        return cooperativaRepository.findById(idCoop)
    }

    /**
     * Obtiene el máximo ID de toda la tabla de cooperativas.
     *
     * @return El máximo ID de la tabla de cooperativas o null si la tabla está vacía.
     */
    fun obtenerMaximoID(): Long? {
        log.debug("Servicio para obtener el máximo ID de toda la tabla")

        return cooperativaRepository.findMaxID()
    }

    /**
     * Inserta una nueva cooperativa.
     *
     * @param cooperativa Objeto [Cooperativa] a insertar.
     * @return Objeto [Cooperativa] insertado o null si no se pudo insertar.
     */
    fun insertarCooperativa(cooperativa: Cooperativa?): Cooperativa? {
        log.debug("Servicio para insertar una cooperativa")

        if (cooperativa == null)
            return null

        val maxID = obtenerMaximoID()
        cooperativa.idCoop = (maxID?.toInt()?.plus(1))?.toLong()!!

        cooperativaRepository.persist(cooperativa)

        if (obtenerCooperativaPorID(cooperativa.idCoop) == null)
            return null

        return cooperativa
    }

    /**
     * Actualiza los datos de una cooperativa.
     *
     * @param cooperativaNuevosDatos Objeto [Cooperativa] con los nuevos datos a actualizar.
     * @return Objeto [Cooperativa] actualizado o null si no se pudo actualizar.
     */
    fun actualizarCooperativa(cooperativaNuevosDatos: Cooperativa): Cooperativa? {
        log.debug("Servicio para actualizar una cooperativa")

        val cooperativaBD = obtenerCooperativaPorID(cooperativaNuevosDatos.idCoop) ?: return null

        cooperativaBD.nombreCoop = cooperativaNuevosDatos.nombreCoop
        cooperativaBD.cantBusesCoop = cooperativaNuevosDatos.cantBusesCoop

        return cooperativaBD
    }

    /**
     * Elimina una cooperativa.
     *
     * @param cooperativaEliminar Objeto [Cooperativa] a eliminar.
     * @return `true` si se eliminó correctamente, `false` si no se encontró la cooperativa.
     */
    fun eliminarCooperativa(cooperativaEliminar: Cooperativa): Boolean {
        log.debug("Servicio para eliminar una cooperativa")

        val cooperativaBD = obtenerCooperativaPorID(cooperativaEliminar.idCoop) ?: return false

        cooperativaRepository.delete(cooperativaBD)

        return true
    }
}
