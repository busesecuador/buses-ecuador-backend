package com.devtech.wsbusesecuador.service


import com.devtech.wsbusesecuador.domain.Persona
import com.devtech.wsbusesecuador.repository.PersonaRepository
import org.slf4j.LoggerFactory
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject
import javax.transaction.Transactional

/**
 * Servicio que proporciona operaciones relacionadas con las personas.
 *
 * @author MateoMartinez
 */
@ApplicationScoped
@Transactional
class PersonaService {

    @Inject
    lateinit var personaRepository: PersonaRepository

    private val log = LoggerFactory.getLogger(javaClass)

    /**
     * Obtiene todas las personas.
     *
     * @return Lista de objetos [Persona] o null si no hay personas disponibles.
     */
    fun obtenerPersonas(): List<Persona>? {
        log.debug("Servicio para obtener todas las personas")

        return personaRepository.findAll().list()
    }

    /**
     * Obtiene una persona por su ID.
     *
     * @param idPers ID de la persona a buscar.
     * @return Objeto [Persona] correspondiente al ID especificado o null si no se encuentra.
     */
    fun obtenerPersonaPorID(idPers: Long): Persona? {
        log.debug("Servicio para obtener una persona por su ID")

        return personaRepository.findById(idPers)
    }

    /**
     * Obtiene el máximo ID de toda la tabla de personas.
     *
     * @return El máximo ID de la tabla de personas o null si la tabla está vacía.
     */
    fun obtenerMaximoID(): Long? {
        log.debug("Servicio para obtener el máximo ID de toda la tabla")

        return personaRepository.findMaxID()
    }

    /**
     * Inserta una nueva persona.
     *
     * @param persona Objeto [Persona] a insertar.
     * @return Objeto [Persona] insertado o null si no se pudo insertar.
     */
    fun insertarPersona(persona: Persona?): Persona? {
        log.debug("Servicio para insertar una persona")

        if (persona == null)
            return null

        val maxID = obtenerMaximoID()
        persona.idPers = (maxID?.toInt()?.plus(1))?.toLong()!!

        personaRepository.persist(persona)

        if (obtenerPersonaPorID(persona.idPers) == null)
            return null

        return persona
    }

    /**
     * Actualiza los datos de una persona.
     *
     * @param personaNuevosDatos Objeto [Persona] con los nuevos datos a actualizar.
     * @return Objeto [Persona] actualizado o null si no se pudo actualizar.
     */
    fun actualizarPersona(personaNuevosDatos: Persona): Persona? {
        log.debug("Servicio para actualizar una persona")

        val personaBD = obtenerPersonaPorID(personaNuevosDatos.idPers) ?: return null

        personaBD.nombrePers = personaNuevosDatos.nombrePers
        personaBD.cedPers = personaNuevosDatos.cedPers
        personaBD.usernamePers = personaNuevosDatos.usernamePers
        personaBD.passwordPers = personaNuevosDatos.passwordPers
        personaBD.correoPers = personaNuevosDatos.correoPers
        personaBD.telfPers = personaNuevosDatos.telfPers
        personaBD.rolPers = personaNuevosDatos.rolPers

        return personaBD
    }

    /**
     * Elimina una persona.
     *
     * @param personaEliminar Objeto [Persona] a eliminar.
     * @return `true` si se eliminó correctamente, `false` si no se encontró la persona.
     */
    fun eliminarPersona(personaEliminar: Persona): Boolean {
        log.debug("Servicio para eliminar una persona")

        val personaBD = obtenerPersonaPorID(personaEliminar.idPers) ?: return false

        personaRepository.delete(personaBD)

        return true
    }

    /**
     * Realiza el inicio de sesión de una persona.
     *
     * @param username Nombre de usuario.
     * @param password Contraseña.
     * @return Objeto [Persona] si el inicio de sesión fue exitoso, null en caso contrario.
     */
    fun login(username: String, password: String): Persona? {
        log.debug("Servicio para iniciar sesión de un usuario con username: $username")

        val personaList = personaRepository.find("username_pers", username).list()

        if (personaList.isEmpty())
            return null
        else {
            if (personaList[0].passwordPers == password)
                return personaList[0]
        }

        return null
    }
}
