package com.devtech.wsbusesecuador.service


import com.devtech.wsbusesecuador.domain.Frecuencia
import com.devtech.wsbusesecuador.repository.FrecuenciaRepository
import org.slf4j.LoggerFactory
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject
import javax.transaction.Transactional

/**
 * Servicio que proporciona operaciones relacionadas con las frecuencias.
 *
 * @author MateoMartinez
 */
@ApplicationScoped
@Transactional
class FrecuenciaService {

    @Inject
    lateinit var frecuenciaRepository: FrecuenciaRepository

    private val log = LoggerFactory.getLogger(javaClass)

    /**
     * Obtiene todas las frecuencias.
     *
     * @return Lista de objetos [Frecuencia] o null si no hay frecuencias disponibles.
     */
    fun obtenerFrecuencias(): List<Frecuencia>? {
        log.debug("Servicio para obtener todas las frecuencias")

        return frecuenciaRepository.findAll().list()
    }

    /**
     * Obtiene una frecuencia por su ID.
     *
     * @param idFrec ID de la frecuencia a buscar.
     * @return Objeto [Frecuencia] correspondiente al ID especificado o null si no se encuentra.
     */
    fun obtenerFrecuenciaPorID(idFrec: Long): Frecuencia? {
        log.debug("Servicio para obtener una frecuencia por su ID")

        return frecuenciaRepository.findById(idFrec)
    }

    /**
     * Obtiene el máximo ID de toda la tabla de frecuencias.
     *
     * @return El máximo ID de la tabla de frecuencias o null si la tabla está vacía.
     */
    fun obtenerMaximoID(): Long? {
        log.debug("Servicio para obtener el máximo ID de toda la tabla")

        return frecuenciaRepository.findMaxID()
    }

    /**
     * Inserta una nueva frecuencia.
     *
     * @param frecuencia Objeto [Frecuencia] a insertar.
     * @return Objeto [Frecuencia] insertado o null si no se pudo insertar.
     */
    fun insertarFrecuencia(frecuencia: Frecuencia?): Frecuencia? {
        log.debug("Servicio para insertar una frecuencia")

        if (frecuencia == null)
            return null

        val maxID = obtenerMaximoID()
        frecuencia.idFrec = (maxID?.toInt()?.plus(1))?.toLong()!!

        frecuenciaRepository.persist(frecuencia)

        if (obtenerFrecuenciaPorID(frecuencia.idFrec) == null)
            return null

        return frecuencia
    }

    /**
     * Actualiza los datos de una frecuencia.
     *
     * @param frecuenciaNuevosDatos Objeto [Frecuencia] con los nuevos datos a actualizar.
     * @return Objeto [Frecuencia] actualizado o null si no se pudo actualizar.
     */
    fun actualizarFrecuencia(frecuenciaNuevosDatos: Frecuencia): Frecuencia? {
        log.debug("Servicio para actualizar una frecuencia")

        val frecuenciaBD = obtenerFrecuenciaPorID(frecuenciaNuevosDatos.idFrec) ?: return null

        frecuenciaBD.idFrec = frecuenciaNuevosDatos.idFrec
        frecuenciaBD.idBusFrec = frecuenciaNuevosDatos.idBusFrec
        frecuenciaBD.habilitadaFrec = frecuenciaNuevosDatos.habilitadaFrec
        frecuenciaBD.fechaFrec = frecuenciaNuevosDatos.fechaFrec

        return frecuenciaBD
    }

    /**
     * Elimina una frecuencia.
     *
     * @param frecuenciaEliminar Objeto [Frecuencia] a eliminar.
     * @return `true` si se eliminó correctamente, `false` si no se encontró la frecuencia.
     */
    fun eliminarFrecuencia(frecuenciaEliminar: Frecuencia): Boolean {
        log.debug("Servicio para eliminar una frecuencia")

        val frecuenciaBD = obtenerFrecuenciaPorID(frecuenciaEliminar.idFrec) ?: return false

        frecuenciaRepository.delete(frecuenciaBD)

        return true
    }
}
