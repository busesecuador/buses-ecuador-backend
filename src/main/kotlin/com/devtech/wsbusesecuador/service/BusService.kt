package com.devtech.wsbusesecuador.service


import com.devtech.wsbusesecuador.domain.Bus
import com.devtech.wsbusesecuador.repository.BusRepository
import org.slf4j.LoggerFactory
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject
import javax.transaction.Transactional

/**
 * Servicio que proporciona operaciones relacionadas con los buses.
 *
 * @author MateoMartinez
 */
@ApplicationScoped
@Transactional
class BusService {

    @Inject
    lateinit var busRepository: BusRepository

    private val log = LoggerFactory.getLogger(javaClass)

    /**
     * Obtiene todos los buses.
     *
     * @return Lista de objetos [Bus] o null si no hay buses disponibles.
     */
    fun obtenerBuses(): List<Bus>? {
        log.debug("Servicio para obtener todos los buses")

        return busRepository.findAll().list()
    }

    /**
     * Obtiene un bus por su ID.
     *
     * @param idBus ID del bus a buscar.
     * @return Objeto [Bus] correspondiente al ID especificado o null si no se encuentra.
     */
    fun obtenerBusPorID(idBus: Long): Bus? {
        log.debug("Servicio para obtener un bus por su ID")

        return busRepository.findById(idBus)
    }

    /**
     * Obtiene el máximo ID de toda la tabla de buses.
     *
     * @return El máximo ID de la tabla de buses o null si la tabla está vacía.
     */
    fun obtenerMaximoID(): Long? {
        log.debug("Servicio para obtener el máximo ID de toda la tabla")

        return busRepository.findMaxID()
    }

    /**
     * Inserta un nuevo bus.
     *
     * @param bus Objeto [Bus] a insertar.
     * @return Objeto [Bus] insertado o null si no se pudo insertar.
     */
    fun insertarBus(bus: Bus?): Bus? {
        log.debug("Servicio para insertar un bus")

        if (bus == null)
            return null

        val maxID = obtenerMaximoID()
        bus.idBus = (maxID?.toInt()?.plus(1))?.toLong()!!

        busRepository.persist(bus)

        if (obtenerBusPorID(bus.idBus) == null)
            return null

        return bus
    }

    /**
     * Actualiza los datos de un bus.
     *
     * @param busNuevosDatos Objeto [Bus] con los nuevos datos a actualizar.
     * @return Objeto [Bus] actualizado o null si no se pudo actualizar.
     */
    fun actualizarBus(busNuevosDatos: Bus): Bus? {
        log.debug("Servicio para actualizar un bus")

        val busBD = obtenerBusPorID(busNuevosDatos.idBus) ?: return null

        busBD.idCoopBus = busNuevosDatos.idCoopBus
        busBD.idChofBus = busNuevosDatos.idChofBus
        busBD.numBus = busNuevosDatos.numBus
        busBD.cantAsienBus = busNuevosDatos.cantAsienBus
        busBD.marcaChasisBus = busNuevosDatos.marcaChasisBus
        busBD.carroceriaBus = busNuevosDatos.carroceriaBus
        busBD.placaBus = busNuevosDatos.placaBus

        return busBD
    }

    /**
     * Elimina un bus.
     *
     * @param busEliminar Objeto [Bus] a eliminar.
     * @return `true` si se eliminó correctamente, `false` si no se encontró el bus.
     */
    fun eliminarBus(busEliminar: Bus): Boolean {
        log.debug("Servicio para eliminar un bus")

        val busBD = obtenerBusPorID(busEliminar.idBus) ?: return false

        busRepository.delete(busBD)

        return true
    }
}
