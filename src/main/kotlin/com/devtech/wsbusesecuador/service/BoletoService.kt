package com.devtech.wsbusesecuador.service


import com.devtech.wsbusesecuador.domain.Boleto
import com.devtech.wsbusesecuador.repository.BoletoRepository
import org.slf4j.LoggerFactory
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject
import javax.transaction.Transactional

/**
 * Servicio que proporciona operaciones relacionadas con los boletos.
 *
 * @author MateoMartinez
 */
@ApplicationScoped
@Transactional
class BoletoService {

    @Inject
    lateinit var boletoRepository: BoletoRepository

    private val log = LoggerFactory.getLogger(javaClass)

    /**
     * Obtiene todos los boletos.
     *
     * @return Lista de objetos [Boleto] o null si no hay boletos disponibles.
     */
    fun obtenerBoletos(): List<Boleto>? {
        log.debug("Servicio para obtener todos los boletos")

        return boletoRepository.findAll().list()
    }

    /**
     * Obtiene un boleto por su ID.
     *
     * @param idBol ID del boleto a buscar.
     * @return Objeto [Boleto] correspondiente al ID especificado o null si no se encuentra.
     */
    fun obtenerBoletoPorID(idBol: Long): Boleto? {
        log.debug("Servicio para obtener un boleto por su ID")

        return boletoRepository.findById(idBol)
    }

    /**
     * Obtiene el máximo ID de toda la tabla de boletos.
     *
     * @return El máximo ID de la tabla de boletos o null si la tabla está vacía.
     */
    fun obtenerMaximoID(): Long? {
        log.debug("Servicio para obtener el máximo ID de toda la tabla")

        return boletoRepository.findMaxID()
    }

    /**
     * Inserta un nuevo boleto.
     *
     * @param boleto Objeto [Boleto] a insertar.
     * @return Objeto [Boleto] insertado o null si no se pudo insertar.
     */
    fun insertarBoleto(boleto: Boleto?): Boleto? {
        log.debug("Servicio para insertar un boleto")

        if (boleto == null)
            return null

        val maxID = obtenerMaximoID()
        boleto.idBol = (maxID?.toInt()?.plus(1))?.toLong()!!

        boletoRepository.persist(boleto)

        if (obtenerBoletoPorID(boleto.idBol) == null)
            return null

        return boleto
    }

    /**
     * Actualiza los datos de un boleto.
     *
     * @param boletoNuevosDatos Objeto [Boleto] con los nuevos datos a actualizar.
     * @return Objeto [Boleto] actualizado o null si no se pudo actualizar.
     */
    fun actualizarBoleto(boletoNuevosDatos: Boleto): Boleto? {
        log.debug("Servicio para actualizar un boleto")

        val boletoBD = obtenerBoletoPorID(boletoNuevosDatos.idBol) ?: return null

        boletoBD.idRutaBol = boletoNuevosDatos.idRutaBol
        boletoBD.idPersBol = boletoNuevosDatos.idPersBol
        boletoBD.idAsienBol = boletoNuevosDatos.idAsienBol
        boletoBD.fechaCompraBol = boletoNuevosDatos.fechaCompraBol
        boletoBD.precioBol = boletoNuevosDatos.precioBol
        boletoBD.comprobPagoBol = boletoNuevosDatos.comprobPagoBol

        return boletoBD
    }

    /**
     * Elimina un boleto.
     *
     * @param boletoEliminar Objeto [Boleto] a eliminar.
     * @return `true` si se eliminó correctamente, `false` si no se encontró el boleto.
     */
    fun eliminarBoleto(boletoEliminar: Boleto): Boolean {
        log.debug("Servicio para eliminar un boleto")

        val boletoBD = obtenerBoletoPorID(boletoEliminar.idBol) ?: return false

        boletoRepository.delete(boletoBD)

        return true
    }
}
