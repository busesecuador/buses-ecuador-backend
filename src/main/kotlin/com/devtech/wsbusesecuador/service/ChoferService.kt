package com.devtech.wsbusesecuador.service


import com.devtech.wsbusesecuador.domain.Chofer
import com.devtech.wsbusesecuador.repository.ChoferRepository
import org.slf4j.LoggerFactory
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject
import javax.transaction.Transactional

/**
 * Servicio que proporciona operaciones relacionadas con los choferes.
 *
 * @author MateoMartinez
 */
@ApplicationScoped
@Transactional
class ChoferService {

    @Inject
    lateinit var choferRepository: ChoferRepository

    private val log = LoggerFactory.getLogger(javaClass)

    /**
     * Obtiene todos los choferes.
     *
     * @return Lista de objetos [Chofer] o null si no hay choferes disponibles.
     */
    fun obtenerChoferes(): List<Chofer>? {
        log.debug("Servicio para obtener todos los choferes")

        return choferRepository.findAll().list()
    }

    /**
     * Obtiene un chofer por su ID.
     *
     * @param idChof ID del chofer a buscar.
     * @return Objeto [Chofer] correspondiente al ID especificado o null si no se encuentra.
     */
    fun obtenerChoferPorID(idChof: Long): Chofer? {
        log.debug("Servicio para obtener un chofer por su ID")

        return choferRepository.findById(idChof)
    }

    /**
     * Obtiene el máximo ID de toda la tabla de choferes.
     *
     * @return El máximo ID de la tabla de choferes o null si la tabla está vacía.
     */
    fun obtenerMaximoID(): Long? {
        log.debug("Servicio para obtener el máximo ID de toda la tabla")

        return choferRepository.findMaxID()
    }

    /**
     * Inserta un nuevo chofer.
     *
     * @param chofer Objeto [Chofer] a insertar.
     * @return Objeto [Chofer] insertado o null si no se pudo insertar.
     */
    fun insertarChofer(chofer: Chofer?): Chofer? {
        log.debug("Servicio para insertar un chofer")

        if (chofer == null)
            return null

        val maxID = obtenerMaximoID()
        chofer.idChof = (maxID?.toInt()?.plus(1))?.toLong()!!

        choferRepository.persist(chofer)

        if (obtenerChoferPorID(chofer.idChof) == null)
            return null

        return chofer
    }

    /**
     * Actualiza los datos de un chofer.
     *
     * @param choferNuevosDatos Objeto [Chofer] con los nuevos datos a actualizar.
     * @return Objeto [Chofer] actualizado o null si no se pudo actualizar.
     */
    fun actualizarChofer(choferNuevosDatos: Chofer): Chofer? {
        log.debug("Servicio para actualizar un chofer")

        val choferBD = obtenerChoferPorID(choferNuevosDatos.idChof) ?: return null

        choferBD.nombreChof = choferNuevosDatos.nombreChof
        choferBD.cedChof = choferNuevosDatos.cedChof

        return choferBD
    }

    /**
     * Elimina un chofer.
     *
     * @param choferEliminar Objeto [Chofer] a eliminar.
     * @return `true` si se eliminó correctamente, `false` si no se encontró el chofer.
     */
    fun eliminarChofer(choferEliminar: Chofer): Boolean {
        log.debug("Servicio para eliminar un chofer")

        val choferBD = obtenerChoferPorID(choferEliminar.idChof) ?: return false

        choferRepository.delete(choferBD)

        return true
    }
}
