package com.devtech.wsbusesecuador.service

import com.devtech.wsbusesecuador.domain.Asiento
import com.devtech.wsbusesecuador.repository.AsientoRepository
import org.slf4j.LoggerFactory
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject
import javax.transaction.Transactional

/**
 * Servicio que proporciona operaciones relacionadas con los asientos.
 * @author MateoMartinez
 */
@ApplicationScoped
@Transactional
class AsientoService {

    @Inject
    lateinit var asientoRepository: AsientoRepository

    private val log = LoggerFactory.getLogger(javaClass)

    /**
     * Obtiene todos los asientos.
     *
     * @return Lista de objetos [Asiento] o null si no hay asientos disponibles.
     */
    fun obtenerAsientos(): List<Asiento>? {
        log.debug("Servicio para obtener todos los asientos")

        return asientoRepository.findAll().list()
    }

    /**
     * Obtiene un asiento por su ID.
     *
     * @param idAsiento ID del asiento a buscar.
     * @return Objeto [Asiento] correspondiente al ID especificado o null si no se encuentra.
     */
    fun obtenerAsientoPorID(idAsiento: Long): Asiento? {
        log.debug("Servicio para obtener un asiento por su ID")

        return asientoRepository.findById(idAsiento)
    }

    /**
     * Obtiene el máximo ID de toda la tabla de asientos.
     *
     * @return El máximo ID de la tabla de asientos o null si la tabla está vacía.
     */
    fun obtenerMaximoID(): Long? {
        log.debug("Servicio para obtener el máximo ID de toda la tabla")

        return asientoRepository.findMaxID()
    }

    /**
     * Inserta un nuevo asiento.
     *
     * @param asiento Objeto [Asiento] a insertar.
     * @return Objeto [Asiento] insertado o null si no se pudo insertar.
     */
    fun insertarAsiento(asiento: Asiento?): Asiento? {
        log.debug("Servicio para insertar un asiento")

        if (asiento == null)
            return null

        val maxID = obtenerMaximoID()
        asiento.idAsien = (maxID?.toInt()?.plus(1))?.toLong()!!

        asientoRepository.persist(asiento)

        if (obtenerAsientoPorID(asiento.idAsien) == null)
            return null

        return asiento
    }

    /**
     * Actualiza los datos de un asiento.
     *
     * @param asientoNuevosDatos Objeto [Asiento] con los nuevos datos a actualizar.
     * @return Objeto [Asiento] actualizado o null si no se pudo actualizar.
     */
    fun actualizarAsiento(asientoNuevosDatos: Asiento): Asiento? {
        log.debug("Servicio para actualizar un asiento")

        val asientoBD = obtenerAsientoPorID(asientoNuevosDatos.idAsien) ?: return null

        asientoBD.idBusAsien = asientoNuevosDatos.idBusAsien
        asientoBD.tipoAsien = asientoNuevosDatos.tipoAsien
        asientoBD.precioAsien = asientoNuevosDatos.precioAsien

        return asientoBD
    }

    /**
     * Elimina un asiento.
     *
     * @param asientoEliminar Objeto [Asiento] a eliminar.
     * @return `true` si se eliminó correctamente, `false` si no se encontró el asiento.
     */
    fun eliminarAsiento(asientoEliminar: Asiento): Boolean {
        log.debug("Servicio para eliminar un asiento")

        val asientoBD = obtenerAsientoPorID(asientoEliminar.idAsien) ?: return false

        asientoRepository.delete(asientoBD)

        return true
    }
}
