package com.devtech.wsbusesecuador.domain

import org.hibernate.Hibernate
import java.io.Serializable
import java.time.LocalDate
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "FRECUENCIAS")
data class Frecuencia(

    @Id
    @Column(name = "ID_FREC")
    var idFrec: Long = 0,

    @Column(name = "ID_COOP_FREC")
    var idCoopFrec: Long = 0,

    @Column(name = "ID_BUS_FREC")
    var idBusFrec: Long = 0,

    @Column(name = "HABILITADA_FREC")
    var habilitadaFrec: String? = null,

    @Column(name = "FECHA_FREC")
    var fechaFrec: LocalDate? = LocalDate.now()

    ) : Serializable {

    companion object {
        private const val serialVersionUID = 1L
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as Frecuencia

        return idFrec == other.idFrec
    }

    override fun hashCode(): Int = javaClass.hashCode()

    override fun toString(): String {
        return "Frecuencia(idFrec=$idFrec, idCoopFrec=$idCoopFrec, idBusFrec=$idBusFrec, " +
                "habilitadaFrec=$habilitadaFrec, fechaFrec=$fechaFrec)"
    }

}