package com.devtech.wsbusesecuador.domain

import org.hibernate.Hibernate
import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "CHOFERES")
data class Chofer(

    @Id
    @Column(name = "ID_CHOF")
    var idChof: Long = 0,

    @Column(name = "NOMBRE_CHOF")
    var nombreChof: String? = null,

    @Column(name = "CED_CHOF")
    var cedChof: String? = null

    ) : Serializable {

    companion object {
        private const val serialVersionUID = 1L
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as Chofer

        return idChof == other.idChof
    }

    override fun hashCode(): Int = javaClass.hashCode()

    override fun toString(): String {
        return "Chofer(idChof=$idChof, nombreChof=$nombreChof, cedChof=$cedChof)"
    }

}