package com.devtech.wsbusesecuador.domain

import org.hibernate.Hibernate
import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "ASIENTOS")
data class Asiento(

    @Id
    @Column(name = "ID_ASIEN")
    var idAsien: Long = 0,

    @Column(name = "ID_BUS_ASIEN")
    var idBusAsien: Long? = 0,

    @Column(name = "TIPO_ASIEN")
    var tipoAsien: String? = null,

    @Column(name = "PRECIO_ASIEN")
    var precioAsien: Float? = 0.0f,

    @Column(name = "NUMERO_ASIEN")
    var numeroAsiento: Long? = 0

    ) : Serializable {

    companion object {
        private const val serialVersionUID = 1L
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as Asiento

        return idAsien == other.idAsien
    }

    override fun hashCode(): Int = javaClass.hashCode()

    override fun toString(): String {
        return "Asiento(idAsien=$idAsien, idBusAsien=$idBusAsien, tipoAsien=$tipoAsien, " +
                "precioAsien=$precioAsien, numeroAsiento=$numeroAsiento)"
    }

}