package com.devtech.wsbusesecuador.domain

import org.hibernate.Hibernate
import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "PERSONAS")
data class Persona(

    @Id
    @Column(name = "ID_PERS")
    var idPers: Long = 0,

    @Column(name = "NOMBRE_PERS")
    var nombrePers: String? = null,

    @Column(name = "CED_PERS")
    var cedPers: String? = null,

    @Column(name = "USERNAME_PERS")
    var usernamePers: String? = null,

    @Column(name = "PASSWORD_PERS")
    var passwordPers: String? = null,

    @Column(name = "CORREO_PERS")
    var correoPers: String? = null,

    @Column(name = "TELF_PERS")
    var telfPers: String? = null,

    @Column(name = "ROL_PERS")
    var rolPers: String? = null

    ) : Serializable {

    companion object {
        private const val serialVersionUID = 1L
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as Persona

        return idPers == other.idPers
    }

    override fun hashCode(): Int = javaClass.hashCode()

    override fun toString(): String {
        return "Persona(idPers=$idPers, nombrePers=$nombrePers, cedPers=$cedPers, usernamePers=$usernamePers, " +
                "passwdPers=$passwordPers, correoPers=$correoPers, telfPers=$telfPers, rolPers=$rolPers)"
    }

}