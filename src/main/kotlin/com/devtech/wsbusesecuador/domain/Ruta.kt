package com.devtech.wsbusesecuador.domain

import org.hibernate.Hibernate
import java.io.Serializable
import java.time.LocalDate
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "RUTAS")
data class Ruta(

    @Id
    @Column(name = "ID_RUTA")
    var idRuta: Long = 0,

    @Column(name = "ID_FREC_RUTA")
    var idFrecRuta: Long = 0,

    @Column(name = "HORA_SALI_RUTA")
    var fechaHoraSaliRuta: LocalDate? = LocalDate.now(),

    @Column(name = "HORA_LLEG_RUTA")
    var fechaHoraLlegRuta: LocalDate? = LocalDate.now(),

    @Column(name = "CIUDAD_INI_RUTA")
    var ciudadIniRuta: String? = null,

    @Column(name = "CIUDAD_FIN_RUTA")
    var ciudadFinRuta: String? = null,

    @Column(name = "PRECIO_RUTA")
    var precioRuta: Float? = 0.0f

    ) : Serializable {

    companion object {
        private const val serialVersionUID = 1L
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as Ruta

        return idRuta == other.idRuta
    }

    override fun hashCode(): Int = javaClass.hashCode()

    override fun toString(): String {
        return "Ruta(idRuta=$idRuta, idFrecRuta=$idFrecRuta, horaSaliRuta=$fechaHoraSaliRuta, " +
                "horaLlegRuta=$fechaHoraLlegRuta, ciudadIniRuta=$ciudadIniRuta, " +
                "ciudadFinRuta=$ciudadFinRuta, precioRuta=$precioRuta)"
    }

}