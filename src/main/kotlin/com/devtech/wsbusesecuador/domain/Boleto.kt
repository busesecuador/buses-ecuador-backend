package com.devtech.wsbusesecuador.domain

import org.hibernate.Hibernate
import java.io.Serializable
import java.time.LocalDate
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "BOLETOS")
data class Boleto(

    @Id
    @Column(name = "ID_BOL")
    var idBol: Long = 0,

    @Column(name = "ID_RUTA_BOL")
    var idRutaBol: Long? = 0,

    @Column(name = "ID_PERS_BOL")
    var idPersBol: Long? = 0,

    @Column(name = "ID_ASIEN_BOL")
    var idAsienBol: Long? = 0,

    @Column(name = "FECHA_COMPRA_BOL")
    var fechaCompraBol: LocalDate? = LocalDate.now(),

    @Column(name = "PRECIO_BOL")
    var precioBol: Float? = 0.0f,

    @Column(name = "COMPROB_PAGO_BOL")
    var comprobPagoBol: String? = null,

    ) : Serializable {

    companion object {
        private const val serialVersionUID = 1L
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as Boleto

        return idBol == other.idBol
    }

    override fun hashCode(): Int = javaClass.hashCode()

    override fun toString(): String {
        return "Boleto(idBol=$idBol, idRutaBol=$idRutaBol, idPersBol=$idPersBol, idAsienBol=$idAsienBol, " +
                "fechaCompraBol=$fechaCompraBol, precioBol=$precioBol, comprobPagoBol=$comprobPagoBol)"
    }

}