package com.devtech.wsbusesecuador.domain

import org.hibernate.Hibernate
import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "BUSES")
data class Bus(

    @Id
    @Column(name = "ID_BUS")
    var idBus: Long = 0,

    @Column(name = "ID_COOP_BUS")
    var idCoopBus: Long = 0,

    @Column(name = "ID_CHOF_BUS")
    var idChofBus: Long = 0,

    @Column(name = "NUM_BUS")
    var numBus: Long = 0,

    @Column(name = "CANT_ASIEN_BUS")
    var cantAsienBus: Int = 0,

    @Column(name = "MARCA_CHASIS_BUS")
    var marcaChasisBus: String? = null,

    @Column(name = "CARROCERIA_BUS")
    var carroceriaBus: String? = null,

    @Column(name = "PLACA_BUS")
    var placaBus: String? = null

    ) : Serializable {

    companion object {
        private const val serialVersionUID = 1L
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as Bus

        return idBus == other.idBus
    }

    override fun hashCode(): Int = javaClass.hashCode()

    override fun toString(): String {
        return "Bus(idBus=$idBus, idCoopBus=$idCoopBus, idChofBus=$idChofBus, numBus=$numBus, " +
                "cantAsienBus=$cantAsienBus, marcaChasisBus=$marcaChasisBus, " +
                "carroceriaBus=$carroceriaBus, placaBus=$placaBus)"
    }

}