package com.devtech.wsbusesecuador.domain

import org.hibernate.Hibernate
import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "COOPERATIVAS")
data class Cooperativa(

    @Id
    @Column(name = "ID_COOP")
    var idCoop: Long = 0,

    @Column(name = "NOMBRE_COOP")
    var nombreCoop: String? = null,

    @Column(name = "CANT_BUSES_COOP")
    var cantBusesCoop: Int? = 0

    ) : Serializable {

    companion object {
        private const val serialVersionUID = 1L
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as Cooperativa

        return idCoop == other.idCoop
    }

    override fun hashCode(): Int = javaClass.hashCode()

    override fun toString(): String {
        return "Cooperativa(idCoop=$idCoop, nombreCoop=$nombreCoop, cantBusesCoop=$cantBusesCoop)"
    }

}