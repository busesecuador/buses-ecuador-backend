package com.devtech.wsbusesecuador.dto

open class EliminadoDTO(
    var eliminado: Boolean = true
) {

    constructor() : this(eliminado = true)

    override fun toString(): String {
        return "EliminadoDTO(eliminado=$eliminado)"
    }

}