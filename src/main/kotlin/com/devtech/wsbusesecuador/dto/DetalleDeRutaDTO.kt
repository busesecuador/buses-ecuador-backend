package com.devtech.wsbusesecuador.dto

import com.devtech.wsbusesecuador.domain.Bus
import com.devtech.wsbusesecuador.domain.Cooperativa
import com.devtech.wsbusesecuador.domain.Ruta
import java.time.LocalDate

open class DetalleDeRutaDTO(
    // RUTA
    var idRuta: Long = 0,
    var idFrecRuta: Long = 0,
    var fechaHoraSaliRuta: LocalDate? = null,
    var fechaHoraLlegRuta: LocalDate? = null,
    var ciudadIniRuta: String? = null,
    var ciudadFinRuta: String? = null,
    var precioRuta: Float? = null,
    // BUS
    var idBus: Long = 0,
    var idCoopBus: Long = 0,
    var idChofBus: Long = 0,
    var numBus: Long = 0,
    var cantAsienBus: Int = 0,
    var marcaChasisBus: String? = null,
    var carroceriaBus: String? = null,
    var placaBus: String? = null,
    // COOPERATIVA
    var idCoop: Long = 0,
    var nombreCoop: String? = null,
    var cantBusesCoop: Int? = 0
) {

    constructor(ruta: Ruta, bus: Bus, cooperativa: Cooperativa) :
            this(ruta.idRuta,
                ruta.idFrecRuta,
                ruta.fechaHoraSaliRuta,
                ruta.fechaHoraLlegRuta,
                ruta.ciudadIniRuta,
                ruta.ciudadFinRuta,
                ruta.precioRuta,
                bus.idBus,
                bus.idCoopBus,
                bus.idChofBus,
                bus.numBus,
                bus.cantAsienBus,
                bus.marcaChasisBus,
                bus.carroceriaBus,
                bus.placaBus,
                cooperativa.idCoop,
                cooperativa.nombreCoop,
                cooperativa.cantBusesCoop
                )

    override fun toString(): String {
        return "DetalleDeRutaDTO(idRuta=$idRuta, idFrecRuta=$idFrecRuta, fechaHoraSaliRuta=$fechaHoraSaliRuta, " +
                "fechaHoraLlegRuta=$fechaHoraLlegRuta, ciudadIniRuta=$ciudadIniRuta, ciudadFinRuta=$ciudadFinRuta, " +
                "precioRuta=$precioRuta, idBus=$idBus, idCoopBus=$idCoopBus, idChofBus=$idChofBus, numBus=$numBus, " +
                "cantAsienBus=$cantAsienBus, marcaChasisBus=$marcaChasisBus, carroceriaBus=$carroceriaBus, placaBus=$placaBus, " +
                "idCoop=$idCoop, nombreCoop=$nombreCoop, cantBusesCoop=$cantBusesCoop)"
    }

}