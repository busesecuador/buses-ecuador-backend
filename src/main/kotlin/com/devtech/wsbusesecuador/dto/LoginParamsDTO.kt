package com.devtech.wsbusesecuador.dto

open class LoginParamsDTO(
    var username: String = "",
    var password: String = ""
) {

    constructor() : this(username = "", password = "")

    override fun toString(): String {
        return "LoginParamsDTO(username='$username', password='$password')"
    }

}